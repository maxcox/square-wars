package ;

import flixel.FlxG;
import flash.geom.Point;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import flixel.util.FlxMath;
import flixel.FlxCamera;
import flash.display.BitmapData;
import haxe.ds.StringMap;

/**
 * ...
 * @author Joseph Cox
 */
class Armor extends Equipment
{

	public var type:String;

	public var midColor:UInt;
	
	public var excludedParts:Array<String>;
	public var partHealth:StringMap<Float>;

	public function new(X:Int, Y:Int, Visual:String, type:String, exclude:Array<String> = null):Void 
	{
		super(X, Y, Visual);
		
		if (exclude != null)
		{
			excludedParts = exclude;
		} else
		{
			excludedParts = new Array<String>();
		}
		
		partHealth = new StringMap<Float>();

		midColor = 0xf4d18f;

		this.type = type;

		if (type == "torso")
		{
			var data:BitmapData = this.sprite.get_pixels();
			midColor = data.getPixel(Std.int(Equipment.graphicSize/2),Std.int(Equipment.graphicSize/2));

			layeroff = .5;
		} else if (type == "legs")
		{
			sprite.animation.destroyAnimations();

			sprite.animation.add("idle",[0], 8, false);
			sprite.animation.add("walk", [1,0,2,0], 8, true);
			sprite.animation.add("sidewalk", [3,2,4,2], 8, true);

			layeroff = -.5;
		} else if (type == "head")
		{
			layeroff = 1;
		}

	}
}