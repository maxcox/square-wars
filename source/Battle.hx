package ;
import body.Bodies;
import body.Body;
import body.BodyPart;
import body.Muscle;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.input.FlxAccelerometer;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxRect;
import haxe.ds.StringMap;
import openfl.geom.Point;

/**
 * ...
 * @author Joseph Cox
 */
class Battle
{
	
	private var head:FlxRect;
	private var uppertorso:FlxRect;
	private var rightupperarm:FlxRect;
	private var leftupperarm:FlxRect;
	private var rightlowerarm:FlxRect;
	private var leftlowerarm:FlxRect;
	private var lowertorso:FlxRect;
	private var rightupperleg:FlxRect;
	private var leftupperleg:FlxRect;
	private var rightlowerleg:FlxRect;
	private var leftlowerleg:FlxRect;
	
	public static var frontrects:StringMap<FlxRect>;
	public static var frontheight:Float;
	public static var frontwidth:Float;
	
	public static var dummyscale:Float;
	
	private var dummy:Body;
	
	public function new() 
	{
		BuildDummy();
	}
	
	public function BuildDummy():Void
	{		
		dummyscale = 20;
		dummy = Bodies.ReturnHumanBody();
		
		head = GetDimensions(Std.int(dummy.parts.get("Head").size * dummyscale), 1.4);
		
		uppertorso = GetDimensions(Std.int(dummy.parts.get("Upper Torso").size * dummyscale), 1.4);
		uppertorso.y = head.y + head.height;
		
		rightupperarm = GetDimensions(Std.int(dummy.parts.get("Right Upper Arm").size * dummyscale), 1.8);
		rightupperarm.x = uppertorso.x - rightupperarm.width;
		rightupperarm.y += head.height / 2 + rightupperarm.height / 2;
		
		leftupperarm = GetDimensions(Std.int(dummy.parts.get("Left Upper Arm").size * dummyscale), 1.8);
		leftupperarm.x = uppertorso.x + uppertorso.width;
		leftupperarm.y += head.height / 2 + leftupperarm.height / 2;
		
		rightlowerarm = GetDimensions(Std.int(dummy.parts.get("Right Lower Arm").size * dummyscale), 3.4);
		rightlowerarm.x = rightupperarm.x - rightlowerarm.width / 4;
		rightlowerarm.y = rightupperarm.y + rightupperarm.height;
		
		leftlowerarm = GetDimensions(Std.int(dummy.parts.get("Left Lower Arm").size * dummyscale), 3.4);
		leftlowerarm.x = leftupperarm.x + leftupperarm.width - leftlowerarm.width * 3 / 4;
		leftlowerarm.y = leftupperarm.y + leftupperarm.height;
		
		lowertorso = GetDimensions(Std.int(dummy.parts.get("Lower Torso").size * dummyscale), .8);
		lowertorso.y = uppertorso.y + uppertorso.height;
		
		rightupperleg = GetDimensions(Std.int(dummy.parts.get("Right Upper Leg").size * dummyscale), 2.8);
		rightupperleg.x = lowertorso.x - rightupperleg.width / 4;
		rightupperleg.y = lowertorso.y + lowertorso.height;
		
		leftupperleg = GetDimensions(Std.int(dummy.parts.get("Left Upper Leg").size * dummyscale), 2.8);
		leftupperleg.x = lowertorso.x + lowertorso.width - leftupperleg.width * 3 / 4;
		leftupperleg.y = rightupperleg.y;
		
		rightlowerleg = GetDimensions(Std.int(dummy.parts.get("Right Lower Leg").size * dummyscale), 3.4);
		rightlowerleg.x = rightupperleg.x;
		rightlowerleg.y = rightupperleg.y + rightupperleg.height;
		
		leftlowerleg = GetDimensions(Std.int(dummy.parts.get("Left Lower Leg").size * dummyscale), 3.4);
		leftlowerleg.x = leftupperleg.x + leftupperleg.width - leftlowerleg.width;
		leftlowerleg.y = leftupperleg.y + leftupperleg.height;
		
		frontrects = new StringMap<FlxRect>();
		frontrects.set("Head", head);
		frontrects.set("Upper Torso", uppertorso);
		frontrects.set("Right Upper Arm", rightupperarm);
		frontrects.set("Left Upper Arm", leftupperarm);
		frontrects.set("Right Lower Arm", rightlowerarm);
		frontrects.set("Left Lower Arm", leftlowerarm);
		frontrects.set("Lower Torso", lowertorso);
		frontrects.set("Right Upper Leg", rightupperleg);
		frontrects.set("Left Upper Leg", leftupperleg);
		frontrects.set("Right Lower Leg", rightlowerleg);
		frontrects.set("Left Lower Leg", leftlowerleg);
		
		frontheight = head.height + uppertorso.height + lowertorso.height + rightupperleg.height + rightlowerleg.height;
		frontwidth = (leftupperarm.x + leftupperarm.width) - rightupperarm.x;
		
		for (i in frontrects)
		{
			var sprite:FlxSprite = new FlxSprite(i.x, i.y);
			sprite.makeGraphic(Std.int(i.width), Std.int(i.height), 0xFF0000FF);
			Layers.DECALS.add(sprite);
		}
	}
	
	private function GetDimensions(size:Int, ratio:Float):FlxRect
	{
		var rect:FlxRect;
		
		var width:Float = Math.sqrt(size / ratio);
		var length:Float = width * ratio;
		
		rect = new FlxRect( -width / 2, -length / 2, width, length);
		
		return rect;
	}
	
	public static function WeaponAttack(creature1:Creature, creature2:Creature, type:String, effectiveness:Float=0.1, attackheight:String="high"):Void
	{
		var weapon:Weapon;
		
		if (creature1.equipment.exists("weapon"))
		{
			weapon = cast creature1.equipment.get("weapon");
		} else
		{
			weapon = creature1.defaultWeapon;
			weapon.checkStats();
			weapon.checkAllAttacks();
		}
		
		var basedamage:Float = weapon.checkAttack(type);
		
		var balancemultiplier:Float = 1.0;
		var attackbalance:Float = Math.sqrt(Math.pow(creature1.balance.x, 2) + Math.pow(creature1.balance.y, 2));
		
		if (attackbalance > 2)
		{
			balancemultiplier = 2 / attackbalance;
			//trace(balancemultiplier);
		}
		
		var finaldamage = basedamage * balancemultiplier;
		
		if (creature1.proficiencies.exists(weapon.type) == true)
		{
			finaldamage += finaldamage * creature1.proficiencies.get(weapon.type);
		}
		
		var worseThanCut:Bool = true;
		var penetrating:Bool = false;
		
		if (type == "stab")
		{
			penetrating = true;
		}
		
		var partname:String = "";
		
		if (type == "stab")
		{
			partname = StabLocation(creature2);
		} else
		{
			partname = FindHitPart(attackheight, type);
		}
		
		if (type == "slash" && weapon.type == Weapon.DAGGER && effectiveness < .9)
		{
			worseThanCut = false;
		} else if (type == "slash")
		{
			if (FlxRandom.chanceRoll(effectiveness*100))
			{
				worseThanCut = true;
			} else
			{
				worseThanCut = false;
			}
		} else if (effectiveness <= 0)
		{
			worseThanCut = false;
		}
		
		if (worseThanCut == true)
		{
			var muscle:Muscle = FindHitMuscle(partname, creature2, penetrating);
			if (muscle != null)
			{
				if (type == "slash" || type == "stab" || type == "cleave")
				{
					if (type == "slash")
					{
						muscle.gash ++;
						
						trace(creature2.name + "'s " + muscle.name + " was split open!");
						
					} else if (type == "stab")
					{
						muscle.stab ++;
						
						if (weapon.type == Weapon.SPEAR)
						{
							muscle.stab ++;
						}
						
						trace(creature2.name + " was stabbed in the " + muscle.name + "!");
					} else
					{
						var cleavechance:Float = (effectiveness * 100) - 20;
						
						if (cleavechance < 0)
						{
							cleavechance = 0;
						}
						
						if (FlxRandom.chanceRoll(cleavechance))
						{
							muscle.cleave ++;
						
							trace(creature2.name + "'s " + muscle.name + " was cleaved from its body!");
						} else
						{
							muscle.gash ++;
						
							trace(creature2.name + "'s " + muscle.name + " was split open!");
						}
					}
					
					Decals.paint(Std.int(creature2.x - creature2.width / 2), Std.int(creature2.y - creature2.height / 2), "bloodtile1.png", FlxRandom.intRanged(0, 360), FlxRandom.floatRanged(0.2, 1.0));
				} else if (type == "strike" || type == "pound")
				{
					var dirx:Float = creature1.x - creature2.x;
					var diry:Float = creature1.y - creature2.y;
					
					creature2.balanceEffect.x -= 5 * FlxMath.signOf(dirx);
					creature2.balanceEffect.y -= 5 * FlxMath.signOf(diry);
					
					//trace(5 * FlxMath.signOf(dirx));
					//trace(5 * FlxMath.signOf(diry));
					
					var disablechance:Float = (effectiveness * 100) - 20;
					
					if (type == "strike")
					{
						disablechance = 20 + (effectiveness * 60);
					}
						
					if (disablechance < 0)
					{
						disablechance = 0;
					}
					
					if (FlxRandom.chanceRoll(disablechance))
					{
						muscle.broke ++;
						
						trace(creature2.name + "s " + muscle.name + " was demolished!");
					} else
					{
						muscle.disable ++;
						
						trace(creature2.name + "'s " + muscle.name + " was disabled!");
					}
				}			
				
				muscle.health = muscle.maxhealth - muscle.gash - muscle.stab - muscle.cleave * 5 - muscle.broke * 5;
				
				if (muscle.health <= 0)
				{
					trace(creature2.name + "'s " + muscle.name + " was destroyed!");
					
					if (muscle.gash >= muscle.maxhealth)
					{
						muscle.cleave ++;
						
						trace(creature2.name + "'s " + muscle.name + " went flying!");
					}
				}
			}
		} else if (partname != "" && (type == "stab" || type == "slash" || type == "cleave") && effectiveness > 0)
		{
			var part:BodyPart = cast creature2.body.parts.get(partname);
			part.cuts ++;
			
			trace(creature2.name + "'s " + part.name + " was sliced!");
		}
		
		if (effectiveness == 0)
		{
			trace(creature1.name + " missed!");
		}
		
		Capabilities.ApplyEffectsAfterDamage(creature2);
	}
	
	public static function FindHitPart(attackheight:String = "high", attacktype:String = "stab", attackdir:String = "left"):String
	{
		var start:FlxPoint = new FlxPoint(0, 0);
		var returnstring:String = "";
		
		if (attackheight == "high")
		{
			start.y = -5 + frontheight / 3 - FlxRandom.intRanged(0, Std.int(frontheight / 3));
		} else
		{
			start.y = -5 + frontheight / 3 + FlxRandom.intRanged(0, Std.int(2*frontheight / 3));
		}
		
		if (attacktype == "stab")
		{
			start.x = FlxRandom.intRanged( -Std.int(frontwidth / 2), Std.int(frontwidth / 2));
		
			var sprite:FlxSprite = new FlxSprite(start.x, start.y);
			sprite.makeGraphic(2, 2, 0xFFFFFFFF);
			Layers.DECALS.add(sprite);
			
			for (k in frontrects.keys())
			{
				var rect:FlxRect = frontrects.get(k);
				if (rect.containsFlxPoint(start))
				{
					returnstring = k;
					break;
				}
			}
		} else if (attacktype == "slash" || attacktype == "strike" || attacktype == "cleave")
		{
			var slashangle:Float = Math.PI / 180 * FlxRandom.floatRanged(135, 215);
			start.x = frontwidth;
			
			
			var testpoint:FlxPoint = start;
			while (testpoint.x > 0 && testpoint.x < 100 && returnstring == "")
			{
				var sprite:FlxSprite = new FlxSprite(testpoint.x, testpoint.y);
				sprite.makeGraphic(2, 2, 0xFFFFFFFF);
				Layers.DECALS.add(sprite);
				
				for (k in frontrects.keys())
				{
					var rect:FlxRect = frontrects.get(k);
					if (rect.containsFlxPoint(start))
					{
						var skip:Bool = false;
						if (k.indexOf("Left") != -1 || k.indexOf("Right") != -1 && k.indexOf("Arm") != -1)
						{
							if (FlxRandom.chanceRoll(50) == true)
							{
								skip = true;
							}
						}
						
						if (skip == false)
						{
							returnstring = k;
							break;
						}
					}
				}
				
				testpoint.x += Math.cos(slashangle) * (dummyscale/2);
				testpoint.y += Math.sin(slashangle) * (dummyscale/2);
			}
		}
		
		return returnstring;
	}
	
	public static function FindHitMuscle(partname:String, creatureAttacked:Creature, penetrating:Bool=false):Muscle
	{
		var hitPart:BodyPart;
		var hitMuscle:Muscle = null;
		
		var totalsize:Int = 0;
		
		if (creatureAttacked.body.parts.exists(partname))
		{
			hitPart = creatureAttacked.body.parts.get(partname);
			
			if (penetrating == true)
			{
				totalsize = Std.int(hitPart.size);
			} else
			{
				totalsize = Std.int(hitPart.exteriorsize);
			}
			
			var rollMuscle:Float = FlxRandom.intRanged(0, totalsize);
			
			var rollCheck:Float = 0;
			
			for (i in hitPart.muscles)
			{
				var muscle:Muscle = cast(i, Muscle);
				
				if (penetrating == true)
				{
					rollCheck += muscle.size;
					
					if (rollMuscle <= rollCheck)
					{
						hitMuscle = muscle;
						break;
					}
				} else if (penetrating == false && muscle.sheltered == false)
				{					
					rollCheck += muscle.size;
					
					if (rollMuscle <= rollCheck)
					{
						hitMuscle = muscle;
						break;
					}
				}
			}
		}
		
		return hitMuscle;
	}
	
	public static function StabLocation(creatureAttacked:Creature):String
	{
		var rollPart:Float = FlxRandom.intRanged(0, Std.int(creatureAttacked.body.totalsize));
		
		var rollCheck:Float = 0;
		
		var hitPart:BodyPart = null;
		
		for (i in creatureAttacked.body.parts)
		{
			var part:BodyPart = cast(i, BodyPart);
			
			rollCheck += part.size;
			
			if (rollPart <= rollCheck)
			{
				hitPart = part;
				break;
			}
		}
		
		return hitPart.name;
	}
	
	/*
	
	public static function Attack(creature1:Creature, creature2:Creature):Void
	{

		var strengthDiff:Int = creature1.STRENGTH - creature2.STRENGTH;
		
		if (strengthDiff > 0)
		{
			
			var muscle:Muscle = HitLocation(creature2);
			
			muscle.health -= Std.int(Math.abs(strengthDiff));
			Capabilities.ApplyEffectsAfterDamage(creature2);
			
			trace(creature2.name + "'s " + muscle.name + " was hit for " + Math.abs(strengthDiff) + " damage!");
			Decals.paint(Std.int(creature2.x - creature2.width / 2), Std.int(creature2.y - creature2.height / 2), "bloodtile1.png", FlxRandom.intRanged(0,360), FlxRandom.floatRanged(0.1,1.0));
			
		} else if (strengthDiff < 0)
		{
			
			var muscle:Muscle = HitLocation(creature1);
			
			muscle.health -= Std.int(Math.abs(strengthDiff));
			Capabilities.ApplyEffectsAfterDamage(creature1);
			
			trace(creature1.name + "'s " + muscle.name + " was hit for " + Math.abs(strengthDiff) + " damage!");
			Decals.paint(Std.int(creature1.x - creature1.width / 2), Std.int(creature1.y - creature1.height / 2), "bloodtile1.png", FlxRandom.intRanged(0,360), FlxRandom.floatRanged(0.1,1.0));
			
		}
	}
	
	public static function HitLocation(creatureAttacked:Creature):Muscle
	{
		
		var rollPart:Float = FlxRandom.intRanged(0, Std.int(creatureAttacked.body.totalsize));
		
		var rollCheck:Float = 0;
		
		var hitPart:BodyPart = null;
		
		for (i in creatureAttacked.body.parts)
		{
			var part:BodyPart = cast(i, BodyPart);
			
			rollCheck += part.size;
			
			if (rollPart <= rollCheck)
			{
				hitPart = part;
				break;
			}
		}
		
		var hitMuscle:Muscle = null;
		
		var rollMuscle:Float = FlxRandom.intRanged(0, Std.int(hitPart.size));
		
		rollCheck = 0;
		
		for (i in hitPart.muscles)
		{
			var muscle:Muscle = cast(i, Muscle);
			
			rollCheck += muscle.size;
			
			if (rollMuscle <= rollCheck)
			{
				hitMuscle = muscle;
				break;
			}
		}
		
		return hitMuscle;
		
	}
	*/
	public static function CheckAggro(creature1:Creature, creature2:Creature):Void 
	{
		
	}
	
}