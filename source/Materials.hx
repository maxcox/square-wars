package;

/**
 * ...
 * @author ...
 */
class Materials
{

	public static var skin:Material = new Material("Skin", 32, .1);
	public static var wood:Material = new Material("Wood", 19.6, .05);
	public static var copper:Material = new Material("Copper", 334.7, .2);
	public static var bronze:Material = new Material("Bronze", 314.9, .5);
	public static var iron:Material = new Material("Iron", 294.1, .7);
	public static var steel:Material = new Material("Steel", 294.1, .8);
	
	public static var swordweightconstant:Float = 1;
	public static var axeweightconstatnt:Float = 1.1;
	
	public static var materialsList:Array<Material> = [skin, wood, copper, bronze, iron, steel];
	
	public function new():Void
	{
	}
	
}