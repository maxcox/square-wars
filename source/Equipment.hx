package ;

import flixel.FlxG;
import flash.geom.Point;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import flixel.util.FlxMath;
import flixel.FlxCamera;

/**
 * ...
 * @author Joseph Cox
 */
class Equipment extends Item
{

	public var sprite:FlxSprite;

	public var layeroff:Float;
	
	public static var graphicSize:Int = 64;

	public function new(X:Int, Y:Int, Visual:String) 
	{
		super(X, Y, Visual);

		sprite = new FlxSprite(X,Y);
		sprite.loadGraphic("assets/images/" + Visual + "_equip.png", true, graphicSize, graphicSize);
		sprite.animation.add("idle", [0], 8, false);
		sprite.animation.add("walk", [1,0,2,0], 8, true);
		sprite.animation.add("sidewalk", [3,0,4,0], 8, true);

		sprite.allowCollisions = 0;

		layeroff = .5;

	}
}