package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.system.FlxCollisionType;
import flixel.tile.FlxTilemap;
import flixel.FlxObject;
import flixel.group.FlxTypedGroup;
import flash.net.SharedObject;
import flash.geom.Point;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxMath;
import flixel.util.FlxRect;
import haxe.ds.IntMap;
import haxe.ds.StringMap;
import openfl.Assets;
import openfl.display.Sprite;

/**
 * ...
 * @author ...
 */
class MapManager
{
	
	/* FUNCTION INDEX:
	
	saveMapData():Void
	
	generateMap(Int):Void
	
	asyncGenerateZone(Point):Void
	
	generateZone(Point):Void
	
	randomTile(Float):Int
	
	prepareSegment(Point):Void
	
	renderSegment(Point,Point):FlxTilemap
	
	reRender(Player):Void
	
	checkSegment(Player):Void
	
	checkZone(Player):Void
	
	update(Player):Void
	*/
	
	// this array gives a list of all eight possible directions on a grid, including diagonally
	public static var directionArray:Array<Point> = [new Point(-1,0), new Point(-1,-1), new Point(0,-1), new Point(1,-1), new Point(1,0), new Point(1,1), new Point(0,1), new Point(-1,1)];

	// manages macro generation and updating for the greater unseen portions of the map
	public var world:WorldManager;
	
	// an object holding the currently rendered portion of the map, holds the instance so that we can easily remove it from display and replace with a new rendered portion
	public var currentSegment:FlxTilemap;

	// the center point of the last rendered portion of the map: basically, where the player was when the map was rendered again
	// helpful for determing if a new render is needed
	public var oldCenter:Point;

	// two dimensional integer map that contains the information for which tiles are displayed at which coordinates of the map
	public var mapData:IntMap<IntMap<Int>>;
	
	// two dimensional integer map that contains information for generation a zone, a null value tells us that this zone of the map has not been generated yet,
	// a zone is a larger chunk of the map that consists of (zoneSize * zoneSize) tiles
	public var mapZones:IntMap<IntMap<ZoneInfo>>;
	
	// a list of all the objects that exist in the world, organized by zone
	public var mapObjects:StringMap<FlxTypedGroup<MapObject>>;
	
	public var mapSprites:FlxTypedGroup<FlxSprite>;
	
	// a manager class to handle generating random objects for each zone
	public var objectGenerator:ObjectGenerator;
	
	// new variable, unused
	public var zonesData:IntMap<IntMap<String>>;

	// the object that connects to our save file for loading and saving map information
	public var mapSave:SharedObject;

	// the width and length of a single tile
	public static var mapTileSize:Int = 32;
	// the width and length of a single zone measured in tiles, zones are generated from 1 to 8 at a time, surrounding whatever zone the player occupies in all directions
	public static var zoneSize:Int = 100;
	
	// half of zoneSize, used to set world point (0,0) in the middle of zone [0][0]
	public static var mapOffset:Point = new Point( -zoneSize / 2, -zoneSize / 2);

	// the width of the screen by tiles, calculated each render in case of a change in stage size, POSSIBLY COULD BE CHANGED
	public var segWidth:Int;
	// the height of the screen by tiles
	public var segHeight:Int;

	// keeps the coordinates of the zone that currently needs to be or is being generated, otherwise it is null
	public var genNeeded:Point;
	
	// necessary for asynchronous generation of zones, it essentially acts as the variable i would in a loop, keeping track of how far the column generation has gotten
	public var genProgressX:Int;
	
	// a boolean checked on each update to see if a new render is needed, this variable is accessed outside of this class
	public var renderNeeded:Bool;
	
	public function new():Void 
	{
		// initialize the macro-generator and macro-updater, THIS WILL BE CHANGED TO ACCOMODATE LOADING AND SAVING OF MACRO ELEMENTS
		world = new WorldManager();
		
		// initializing the save file by getting or creating the local file by the name of the string argument
		mapSave = SharedObject.getLocal("squarewarsmap1");
		
		mapObjects = new StringMap<FlxTypedGroup<MapObject>>();
		mapSprites = new FlxTypedGroup<FlxSprite>();
		
		objectGenerator = new ObjectGenerator();
		
		/* this paragraph can be uncommented to erase saved data
		 */ 
		mapSave.data.map = null;
		mapSave.data.offset = null;
		mapSave.data.zones = null;
		
		mapSave.data.zonesdata = null;
		mapSave.data.mapregions = null;
		
		mapSave.flush();
		
		// if the saved map data exists, then load all our map variables from the save file
		if (mapSave.data.map != null)
		{
			mapData = mapSave.data.map;
			mapZones = mapSave.data.zones;
			
			zonesData = mapSave.data.zonesdata;
			world.mapRegions = mapSave.data.mapregions;
			
		} else
		{
			// then generate the first zone of the new map
			generateMap();
			// trace(mapData);
		}
		
		// prepare the first segment for rendering and render, right now it uses 0,0 as the starting point because we are not loading the player's saved position, WILL BE CHANGED
		prepareSegment(new Point(0,0));

		// set genNeeded to null to indicate no generation is needed yet, as well as to initialize it
		genNeeded = null;
		
		// set our two async looping variables to zero
		genProgressX = 0;
		
		// initializing renderNeeded variable with the value false because prepareSegment already rendered the first segment
		renderNeeded = false;
	}

	public function saveMapData():Void
	{
		// set the saved data objects to the values of our local variables
		mapSave.data.map = mapData;
		mapSave.data.zones = mapZones;
		
		mapSave.data.zonesdata = zonesData;
		mapSave.data.mapRegions = world.mapRegions;
		
		// write our new saved data to our save file
		mapSave.flush();
	}

	//GENERATING FUNCTIONS//
	//
	public function generateMap(?totalTiles:Int):Void
	{
		var zoneID:String = Std.string( ( -WorldManager.regionOffset.x) + ":" + ( -WorldManager.regionOffset.y));
		trace(zoneID);
		
		// set totalTiles to zoneSize to generate one zone, PROBABLY WILL TAKE OUT THE ARGUMENT AS WELL AS THE NULL CHECK
		if (totalTiles == null)
		{
			totalTiles = zoneSize;
		}
		
		// initialize all of our local map data variables in their empty states
		mapData = new IntMap<IntMap<Int>>();
		
		zonesData = new IntMap<IntMap<String>>();
		
		world.generateFirst();
		
		mapZones = new IntMap<IntMap<ZoneInfo>>();
		mapZones.set(Std.int(-WorldManager.regionOffset.x), new IntMap<ZoneInfo>());
		mapZones.get(Std.int( -WorldManager.regionOffset.x)).set(Std.int( -WorldManager.regionOffset.y), new ZoneInfo(world.mapRegions.get("0,0")));
		
		var minimumTilesX:Int = Std.int((0) * zoneSize);
		var minimumTilesY:Int = Std.int((0) * zoneSize);
		
		var minimumPixelsX:Int = Std.int((minimumTilesX + MapManager.mapOffset.x) * MapManager.mapTileSize);
		var minimumPixelsY:Int = Std.int((minimumTilesY + MapManager.mapOffset.y) * MapManager.mapTileSize);
		
		var zonePixelSize:Int = zoneSize * mapTileSize;
		
		mapObjects.set(zoneID, objectGenerator.generateObjects(new FlxRect(minimumPixelsX, minimumPixelsY, zonePixelSize, zonePixelSize)));
		
		//initialize tile value integer object for use in loop
		var tile:Int = 1;
		//initialize tileChance float object for holding randomly generated float
		var tileChance:Float;
		
		for (ix in 0...totalTiles)
		{
			// initialize columns
			mapData.set(ix, new IntMap<Int>());
			for (iy in 0...totalTiles)
			{
				// set tileChance to a random float between 0.0 and 1.0
				tileChance = FlxRandom.float();
				
				// check what tile the float indicates and assign that value to the tile int
				tile = randomTile(tileChance);
				
				// set the map coordinate data to hold the randoly generated tile integer
				mapData.get(ix).set(iy, tile);
			}
		}
		
		// save the new map data object that now includes the newly generated zone
		saveMapData();
	}
	
	// ASYNCHRONOUS GENERATION
	public function asyncGenerateZone(zonePoint:Point):Void
	{
		// zpx and zpy translate zonePoint (the value of the zone we are generating) into two easily usable integers
		var zpx:Int = Std.int(zonePoint.x);
		var zpy:Int = Std.int(zonePoint.y);
		
		// the coordinates to start generation from, described by their tile value
		var startx:Int = Std.int((zpx + WorldManager.regionOffset.x) * zoneSize);
		var starty:Int = Std.int((zpy + WorldManager.regionOffset.y) * zoneSize);
		
		//trace(startx, starty);
		
		// integer object to hold generated tiles' values
		var tile:Int = 1;
		
		// float to hold randomly generated float for choosing tile values
		var tileChance:Float;
		
		// this integer determines how big of a chunk of the zone to generate per incremental update, the bigger the number zoneSize is divided by, the slower the generation happens
		var progressSegment:Int = Std.int(zoneSize / 10);
		
		// a loop that cycles through the progressSegment-determined amount of the zone that is being generated this update
		for (ix in 0...progressSegment)
		{
			// this integer determines the x coordinate in tiles of the tile we are generating
			var ixx:Int = startx + ix + genProgressX;
			
			// if the column doesn't exist in our map data variable, initialize a new column
			if (mapData.exists(ixx) == false)
			{
				mapData.set(ixx, new IntMap<Int>());
			}
			
			// a loop cycling through the entire vertical size of a zone, so this entire function is an async generation of progressSegment-amount of columns at a time
			for (iy in 0...zoneSize)
			{
				// determines the y coordinate in tiles of the tile we are generating
				var iyy:Int = starty + iy;
				
				// set the tileChance to a random float between 0.0 and 1.0
				tileChance = FlxRandom.float();
				
				// take the tile value that corresponds with our tileChance float and put it into our tile integer object
				tile = randomTile(tileChance);
				
				// insert the generated tile information into our map data object by our tile's coordinates
				mapData.get(ixx).set(iyy, tile);
			}
		}
		
		// increment our looping variable by the amount of columns we just generated
		genProgressX += progressSegment;
		
		// if the progress variable is greater than the zone size, then our zone has been successfully generated
		if (genProgressX >= zoneSize)
		{
			// set our looping variable to zero for the next round of asynchronous generation
			genProgressX = 0;
			// set our currently needed generation zone to null so our map manager knows nothing needs to be generated
			genNeeded = null;
			
			// if the column our new zone inhabits by it's zone-scale coordinates does not exist, initialize the column object within our zone tracking object
			if (mapZones.exists(zpx) == false)
			{
				mapZones.set(zpx, new IntMap<ZoneInfo>());
			}
			
			var rpx:Int = Math.floor(zpx / WorldManager.regionSize);
			var rpy:Int = Math.floor(zpy / WorldManager.regionSize);
			
			// tell our map manager that the zone has been generated at these zone-scale coordinates (zpx, zpy) by inserting a true value within our zone tracking object
			mapZones.get(zpx).set(zpy, new ZoneInfo(world.mapRegions.get(Std.string(rpx + "," + rpy))));
		
			var minimumTilesX:Int = Std.int((zpx + WorldManager.regionOffset.x) * zoneSize);
			var minimumTilesY:Int = Std.int((zpy + WorldManager.regionOffset.y) * zoneSize);
		
			var minimumPixelsX:Int = Std.int((minimumTilesX + MapManager.mapOffset.x) * MapManager.mapTileSize);
			var minimumPixelsY:Int = Std.int((minimumTilesY + MapManager.mapOffset.y) * MapManager.mapTileSize);
			
			var zonePixelSize:Int = zoneSize * mapTileSize;
			
			var zoneID:String = Std.string(zpx + ":" + zpy);
			
			mapObjects.set(zoneID, objectGenerator.generateObjects(new FlxRect(minimumPixelsX, minimumPixelsY, zonePixelSize, zonePixelSize)));
			
			trace(zpx, zpy);
		} 
	}
	//

	// exactly the same as asynchronous generation, but it does it all in one go
	public function generateZone(zonePoint:Point):Void
	{
		var zpx:Int = Std.int(zonePoint.x);
		var zpy:Int = Std.int(zonePoint.y);

		var startx:Int = 0;
		var starty:Int = 0;

		startx = Std.int(zpx*zoneSize);
		starty = Std.int(zpy*zoneSize);

		//trace(startx, starty);

		var tile:Int = 1;
		var tileChance:Float;

		for (ix in 0...zoneSize)
		{
			var ixx:Int = startx + ix;
			if (mapData.exists(ixx) == false)
			{
				mapData.set(ixx, new IntMap<Int>());
			}

			for (iy in 0...zoneSize)
			{
				var iyy:Int = starty + iy;

				tileChance = FlxRandom.float();
				
				tile = randomTile(tileChance);

				mapData.get(ixx).set(iyy, tile);
			}
		}
		
		if (mapZones.exists(zpx) == false)
		{
			mapZones.set(zpx, new IntMap<ZoneInfo>());
		}
			
		var rpx:Int = Math.floor(zpx / WorldManager.regionSize);
		var rpy:Int = Math.floor(zpy / WorldManager.regionSize);
		
		mapZones.get(zpx).set(zpy, new ZoneInfo(world.mapRegions.get(Std.string(rpx + "," + rpy))));
		
		genNeeded = null;
	}
	//
	
	// finds a random tile integer value based on its corresponding float value between 0.0 and 1.0, THIS WILL DEFINITELY BE CHANGED, THIS IS ONLY A SIMPLIFIED TEST VERSION
	private function randomTile(tileChance:Float):Int
	{
		// 1 is the most commonplace tile, it is the ground tile
		if (tileChance < .3)
		{
			return 1;
		} else if (tileChance < .5)
		{
			return 2;
		} else if (tileChance < .8)
		{
			return 3;
		} else if (tileChance < .99)
		{
			return 4;
		}else if (tileChance < .999999)
		{
			return 5;
		} else
		{
			return 1;
		}
	}
	//
	//end GENERATING FUNCTIONS//

	//RENDER FUNCTIONS//
	//
	// prepare the information for a segment to be rendered and pass that information to the renderSegment function 
	public function prepareSegment(startPoint:Point):Void
	{
		// set the width in tiles of a segment that spans the width of the screen
		segWidth = Math.floor(FlxG.width / mapTileSize);
		// set the height of the screen in tiles
		segHeight = Math.floor(FlxG.height/mapTileSize);
		
		// take half of segWidth, used to create a buffer of rendered tiles on the edge of screen
		var halfsegmentx:Int = Math.floor(segWidth/2);
		// take half of segHeight, used to create a buffer of rendered tiles on the edge of screen
		var halfsegmenty:Int = Math.floor(segHeight/2);

		// the x coordinate in pixels of the left corner of the tile that contains startPoint
		var sx:Float = startPoint.x - startPoint.x%mapTileSize;
		// the y coordinate in pixels of the left corner of the tile that contains startPoint
		var sy:Float = startPoint.y - startPoint.y%mapTileSize;

		// the x coordinate in tiles of the startPoint: the x value of the tile in the center of the render
		var starttilex:Int = Std.int(sx / mapTileSize/*);//*/ - mapOffset.x);
		// the y coordinate in tiles of the startPoint: the y value of the tile in the center of the render
		var starttiley:Int = Std.int(sy / mapTileSize/*);//*/ - mapOffset.y);

		// the actual pixel coordinates of the leftmost corner of the segment to be rendered, used to position the FlxTilemap generated by renderSegment():FlxTilemap
		var coordinates:Point = new Point(startPoint.x - halfsegmentx*mapTileSize - segWidth*mapTileSize - (startPoint.x%mapTileSize), startPoint.y - halfsegmenty*mapTileSize - segHeight*mapTileSize - (startPoint.y%mapTileSize));

		// coordinates in tiles describing the leftmost corner of the segment to be rendered, used to start the loop for displaying tiles
		var minimumTile:Point = new Point(starttilex - halfsegmentx - segWidth, starttiley - halfsegmenty - segHeight);
		
		// calls our segment rendering function, and sets the newly rendered segment to our currentSegment container
		currentSegment = renderSegment(coordinates, minimumTile);

		// setting our oldCenter point to our most recent startPoint, or the center point of our most recent render
		oldCenter = startPoint;
	}

	// use the information prepared in prepareSegment to create an FlxTilemap and fill it with the correct range of tiles to be rendered
	public function renderSegment(coords:Point, minimum:Point):FlxTilemap
	{
		// initializing the FlxTilemap object to be filled with tiles, added to the display, and returned to be placed in the currentSegment container
		var mapSegment:FlxTilemap = new FlxTilemap();
		
		// initializing the array of tile identities to be passed into our FlxTilemap object
		var segmentData:Array<Int> = new Array<Int>();
		
		// this loop is backwards so that filling the array goes rows first instead of columns, as that is the required order for FlxTilemap
		// the rendered segment will be 3 times the height and width of the screen
		for (iy in 0...(segHeight*3))
		{
			// setting the row number with the y value in tiles of the leftmost corner of the segment to be rendered
			var iyy:Int = iy + Std.int(minimum.y);

			for (ix in 0...(segWidth*3))
			{
				// setting the column number with the x value in tiles of the leftmost corner of the segment to be rendered
				var ixx:Int = ix + Std.int(minimum.x);
				
				// check if any tile data exists at these coordinates
				if (mapData.exists(ixx) == true)
				{
					// the tile object received from our map data by its coordinates
					var tile:Int = mapData.get(ixx).get(iyy);

					if (tile > 0)
					{
						// the tile exists and is placed in our array
						segmentData.push(tile);
					} else
					{
						// this tile is empty
						segmentData.push(0);
					}
				} else
				{
					// this tile is empty
					segmentData.push(0);
				}
			}
		}

		// telling the FlxTilemap object its width and length so it knows how to read and display the one-dimensional array of tiles we are sending it
		mapSegment.widthInTiles = segWidth*3;
		mapSegment.heightInTiles = segHeight*3;
		
		// we are now passing our array of tile information into the FlxTilemap object to be loaded, this includes our tileset image as well as the pixel width and height of our tiles
		mapSegment.loadMap(segmentData, "assets/images/grass/tile1.png", mapTileSize, mapTileSize);

		// setting specific tile properties for collisions, ANY means objects will collide with it, NONE means they will not
		mapSegment.setTileProperties(1, FlxObject.NONE);
		mapSegment.setTileProperties(2, FlxObject.NONE);
		mapSegment.setTileProperties(3, FlxObject.NONE);
		mapSegment.setTileProperties(4, FlxObject.NONE);

		// setting the main visual position of our FlxTilemap object in pixels, this uses the coords variable passed in from prepareSegment
		mapSegment.setPosition(coords.x, coords.y);

		// adding the mapSegment to our background layer to be displayed visually
		Layers.BACKGROUND.add(mapSegment);

		// returning our mapSegment so that prepareSegment can set it in the currentSegment container
		return mapSegment;
	}

	// this function removes our currently rendered segment from visual display and tells the map manager to render a new version based on the player's current position
	public function reRender(player:Player):Void
	{
		// remove the currenly rendered segment from visual display
		Layers.BACKGROUND.remove(currentSegment);
		
		// start the rendering process again, passing in the player's current coordinates as the center point of the to-be rendered segment
		prepareSegment(new Point(player.x, player.y));
	}
	//
	//end RENDER FUNCTIONS//

	//UPDATE FUNCTIONS//
	//
	// checks whether the player has gone far enough for a new segment of the map to be rendered
	public function checkSegment(player:Player):Void
	{
		// if the horizontal distance between the player's position and the center of the last rendered segment is greater than half the width of the screen,
		// tell the map manager that a new render is needed
		if (Math.abs(player.x - oldCenter.x) > FlxG.width/2)
		{
			renderNeeded = true;
		} else if (Math.abs(player.y - oldCenter.y) > FlxG.height/2)
		{
			renderNeeded = true;
		} 
		// ^^^^^^^^^^^^^^ also checks vertical distance compared to half the height of the screen
	}

	// checks all zones surrounding the player's current zone to see if any of them need to be generated
	// if they do, genNeeded is set to the coordinates of the zone that requires generation
	public function checkZone(player:Player):Void
	{
		// sets to px and py the coordinate values of the left most corner of the tile the player occupies
		var px:Float = Math.floor(player.x);
		var py:Float = Math.floor(player.y);
		
		// finds the coordinates in tiles of the tile the player occupies and sets to playertilex and playertiley
		var playertilex:Int = Std.int(px / mapTileSize - mapOffset.x);
		var playertiley:Int = Std.int(py / mapTileSize - mapOffset.y);
		
		// finds the coordinates in zones of the zone the player occupies and sets to playerzonex and playerzoney
		var playerzonex:Int = Math.floor(playertilex / zoneSize - WorldManager.regionOffset.x);
		var playerzoney:Int = Math.floor(playertiley / zoneSize - WorldManager.regionOffset.y);
		
		// this loop uses our 8 direction array to check the existence of all the zones surrounding the zone the player occupies
		for (i in directionArray)
		{	
			// zoneintx and zoneinty hold the coordinates in zones of the zone being checked for existence
			var zoneintx:Int = Std.int(i.x + playerzonex);
			var zoneinty:Int = Std.int(i.y + playerzoney);
			
			// if the column of the zone being checked does not exist, or if the column exists but the actual zone at the coordinates does not,
			// genNeeded is set to the coordinates of the zone we are checking
			if (mapZones.exists(zoneintx) == false)
			{
				genNeeded = new Point(zoneintx, zoneinty);
				break;
			} else if (mapZones.get(zoneintx).exists(zoneinty) == false)
			{
				genNeeded = new Point(zoneintx, zoneinty);
				break;
			}
		}
	}
	
	private function getZone(checkPoint:Point):Point
	{
		var zone:Point;
		
		// sets to px and py the coordinate values of the left most corner of the tile the point occupies
		var px:Float = Math.floor(checkPoint.x);
		var py:Float = Math.floor(checkPoint.y);
		
		// finds the coordinates in tiles of the tile the point occupies and sets to playertilex and playertiley
		var pointtilex:Int = Std.int(px / MapManager.mapTileSize - MapManager.mapOffset.x);
		var pointtiley:Int = Std.int(py / MapManager.mapTileSize - MapManager.mapOffset.y);
		
		// finds the coordinates in zones of the zone the point occupies and sets to playerzonex and playerzoney
		var pointzonex:Int = Math.floor(pointtilex / MapManager.zoneSize - WorldManager.regionOffset.x);
		var pointzoney:Int = Math.floor(pointtiley / MapManager.zoneSize - WorldManager.regionOffset.y);
		
		zone = new Point(pointzonex, pointzoney);
		
		return zone;
	}
	
	public function objectsUpdate(player:Player):Void
	{
		var currentZone:Point = getZone(new Point(player.x, player.y));
		
		var distancex:Float = FlxG.width;
		var distancey:Float = FlxG.height;
		
		var minx:Float = player.x - distancex;
		var miny:Float = player.y - distancey;
		
		var maxx:Float = player.x + distancex;
		var maxy:Float = player.y + distancey;
		
		function checkForRender(object:MapObject):Void
		{
			var layer:FlxGroup;
			
			if (object.z == 0)
			{
				layer = Layers.UNDERGROUND;
			} else
			{
				layer = Layers.FOREGROUND;
			}
			
			if (object.x > minx && object.y > miny)
			{
				if (object.x < maxx && object.y < maxy)
				{
					if (object.rendered == false)
					{
						var renderSprite:FlxSprite = new FlxSprite(object.x, object.y, "assets/images/" + object.id + ".png");
						
						renderSprite.setGraphicSize(Std.int(renderSprite.width * object.scale), Std.int(renderSprite.height * object.scale));
						renderSprite.updateHitbox();
						renderSprite.width *= object.hitsize;
						renderSprite.height *= object.hitsize;
						renderSprite.centerOffsets();
						renderSprite.angle = object.rot;
						renderSprite.immovable = true;
						
						layer.add(renderSprite);
						
						object.sprite = renderSprite;
						
						object.rendered = true;
					}
				} else if (object.rendered == true)
				{
					layer.remove(object.sprite);
					object.sprite.destroy();
					object.sprite = null;
					
					object.rendered = false;
				}
			} else if (object.rendered == true)
			{
				layer.remove(object.sprite);
				object.sprite.destroy();
				object.sprite = null;
					
				object.rendered = false;
			}
		}
		
		var zoneID:String = Std.string(currentZone.x + ":" + currentZone.y);
		if (mapObjects.exists(zoneID))
		{
			var objectsToUpdate:FlxTypedGroup<MapObject> = mapObjects.get(zoneID);
			objectsToUpdate.forEach(checkForRender);
		}
	}
	//
	//end UPDATE FUNCTIONS///
	
	public function update(player:Player):Void
	{
		// if the boolean renderNeeded is false, we are checking to see if a new render is needed, otherwise we render a new segment, destroying the old one in the process
		if (renderNeeded == false)
		{
			// check if the player has moved enough to require a new segment to be rendered
			checkSegment(player);
		} else
		{
			// render the new segment based on the current position of the player
			reRender(player);
			// set renderNeeded to false so the map manager knows a new render is not yet required until another check is made
			renderNeeded = false;
		}

		// if genNeeded is null, we are checking to see if a new zone needs to be generated, otherwise we are generating a new zone asynchronously
		if (genNeeded == null)
		{
			// check if the player has moved to a new zone and so requires the zones surrounding to be generated
			checkZone(player);
		} else
		{
			// generate tiles in the zone that needs generation asynchronously until it is full of tiles
			asyncGenerateZone(genNeeded);
		}
		
		// update macro generation, life algorithms, and creature display checks
		//world.update(player);
		
		objectsUpdate(player);
	}
}