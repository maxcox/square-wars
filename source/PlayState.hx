package;

import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.FlxBasic;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxRandom;
import flixel.util.FlxRect;
import haxe.ds.StringMap;
import openfl.geom.Point;
import openfl.display.FPS;
import openfl.display.InterpolationMethod;
import flixel.system.scaleModes.StageSizeScaleMode;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	/**
	 * Function that is called up when the state is created to set it up. 
	 */
	
	// main instance of FlxCamera for reference
	public var camera:FlxCamera;
	
	// hud shows the player's blood and oxygen levels, should show any other important information in the future
	public var hud:Hud;
	
	// the player object, handles all player control options
	public var player:Player;
	
	// an NPC object for testing, handles NPC AI
	public var enemy:NPC;
	
	// the physical maptiles object, updated and displayed through MapManager.hx
	public var mapsegment:FlxTilemap;
	
	// filters special class extends FlxGroup, for adding effects overtop game, i.e. blinding effects and vision cones
	public var filters:Filters;
	
	// battle manager handles hit queries to determine which body part was hit per each attack, mostly consists of static functions
	// but requires new() to be called to function properly: that's why we're seeing it here
	public var battlemanager:Battle;
	
	// map manager handles all map generation, saving, loading, and positioning math: most of its functions take Player as the only argument
	public var map:MapManager;
	
	override public function create():Void
	{
		super.create();
		
		// Markov.hx generates new random names based on the input: here it is generating last names and american male first names
		//Markov.loadArray(Markov.lastNames, Markov.lastNamesTable, Markov.lastNamesStartTable);
		//Markov.loadArray(Markov.americanMaleNames, Markov.americanMaleNamesTable, Markov.americanMaleNamesStartTable);
		//Markov.loadArray(Markov.countryNames, Markov.countryNamesTable, Markov.countryNamesStartTable);
		
		// initializing battle manager
		battlemanager = new Battle();
		
		// setting general mouse settings
		FlxG.mouse.useSystemCursor = true;
		FlxG.mouse.visible = true;
		
		// initializing map manager: this will check if a previous map is saved and will either load that map or generate a new one
		map = new MapManager();
		
		for (i in 0...20)
		{
			//trace(Markov.getCountry());
		}
		
		// this sets flixel's worldbounds to a gigantic number just in case, PROBABLY CAN BE REMOVED
		FlxG.worldBounds.set( -100000, -100000, 200000, 200000);
		
		// initializing our camera variable as flixel's main camera, setting camera style to lockon, and setting the default bg to white
		camera = FlxG.camera;
		camera.style = FlxCamera.STYLE_LOCKON;
		FlxG.cameras.bgColor = FlxColor.WHITE;
		
		// this paragraph initializes our player, most will be replaced later by a character creation menu
		// and load/save operation for pulling saved character data
		player = new Player( 0, 0, "paul64");
		// this sets our player's stats as default, THIS WILL BE REPLACED
		player.setDefaultStats();
		// this sets our player's name by chosing random generated names from our Markov.hx
		player.name = "James";//Markov.getFirstName() + " " + Markov.getLastName();
		// setting our player type to Human, has no effect at this time, POSSIBLE REMOVE
		player.type = "Human";
		
		// this is a character proficiencies test, in which the character's proficiency in swordplay is set to .1
		player.proficiencies.set(Weapon.SWORD, .1);
		// this adds the player to the world visually as well as attaching it to the updating system
		CreatureManager.add(player);
		
		/*
		//create three enemies for combat testing
		for ( i in 0...3)
		{
			// enemy initialized and positioned randomly
			enemy = new NPC(Std.int(50 + 20 + i * 10), 50 + 20 + i * FlxRandom.intRanged(0, 20), "paul64");
			// enemy's angle is randomized
			enemy.angle = FlxRandom.intRanged( -180, 180);
			// enemy's stats are randomized
			enemy.setRandomStats();
			// enemy's name chosen randomly
			enemy.name = "Paul";// Markov.getFirstName() + " " + Markov.getLastName();
			// enemy type set to Human, not important
			enemy.type = "Human";
			// a random one handed sword is generated
			var enemyweapon:Weapon = Weapon.randomOneHandSword();
			// the weapon is equipped
			enemy.equip(enemyweapon);
			// display the random weapon's stats
			enemyweapon.checkStats();
			// add enemy to the game
			CreatureManager.add(enemy);
			
			// tell enemy to follow the player
			enemy.setFollowing(player);
		}
		*/
		
		// sets the camera to follow the player
		camera.target = player;
		
		// this chunk adds all visual layers in their proper sequence so as to avoid overlapping errors
		add(Layers.BACKGROUND);
		add(Layers.UNDERGROUND);
		add(Layers.DECALS);
		add(Layers.LOOT);
		add(Layers.CREATURES);
		add(Layers.EFFECTS);
		add(Layers.FOREGROUND);
		add(Layers.FILTERS);
		//possibly combine playerhud and buttons layers
		add(Layers.PLAYERHUD);
		#if mobile
		add(Layers.BUTTONS);
		#end
		//

		// setting scaling so the stage can change size without effecting zoom or ratios in-game
		FlxG.scaleMode = new StageSizeScaleMode();
		
		// displaying the player's default weapon's (its fist's) stats
		player.defaultWeapon.checkStats();
		
		// initialize the hud with the player instance as the argument
		hud = new Hud(player);
		// add the hud object visually to our playerhud display layer
		Layers.PLAYERHUD.add(hud);
		
		trace(CreatureManager.ActiveCreatures);
	}

	override public function onResize(width:Int, height:Int):Void
	{
		super.onResize(width,height);

		// tell the map manager that the map needs to be rendered again after the screen size is changed
		map.renderNeeded = true;
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		if (player.paused == false)
		{
			super.update();
			
			// set the flixel's functional world bounds to be whatever the camera can see
			FlxG.worldBounds.set(Math.round(FlxG.camera.target.x - FlxG.camera.width), Math.round(FlxG.camera.target.y - FlxG.camera.height), FlxG.camera.width * 3, FlxG.camera.height * 3);
			
			// check if any active creatures are colliding and separate them
			FlxG.collide(CreatureManager.ActiveCreatures, CreatureManager.ActiveCreatures);
			
			// this function fixes a glitch in creature balance when running into a tile, THIS SHOULD BE REMOVED BUT NOT SURE HOW YET
			function collisionFix(creature:Creature, tile:Dynamic):Void
			{
				creature.RenderBalance();
			}
			
			function alphaCheck(basic:FlxBasic):Void
			{
				var sprite:FlxSprite = cast basic;
				
				var rwidth:Float = sprite.frameWidth * sprite.scale.x * .8;
				var rheight:Float = sprite.frameHeight * sprite.scale.y * .8;
				
				var rect:FlxRect = new FlxRect(sprite.x + sprite.width - rwidth/2, sprite.y + sprite.height - rheight/2, rwidth, rheight);
				
				sprite.alpha = 1;
				
				for (c in CreatureManager.ActiveCreatures)
				{
					var crect:FlxRect = new FlxRect(c.x + c.width/2, c.y + c.height/2, c.width, c.height);
					if (rect.overlaps(crect))
					{
						sprite.alpha = .3;
					}
				}
			}
			// check if any active creatures are colliding with world tiles, separate them, and perform earlier mentioned fix
			//FlxG.collide(CreatureManager.ActiveCreatures, map.currentSegment, collisionFix);
			FlxG.collide(CreatureManager.ActiveCreatures, Layers.FOREGROUND, collisionFix);
			FlxG.collide(CreatureManager.ActiveCreatures, Layers.UNDERGROUND, collisionFix);
			
			Layers.FOREGROUND.forEach(alphaCheck);
			
			// update the map's render space based on the player's new position, this also checks if more map generation is needed
			map.update(player);
		} else if (player.paused == true)
		{
			// if the player has paused, its updating is stopped, so we need this code here to allow an unpause when the space key is released, PROBABLY SHOULD BE REMOVED
			if (FlxG.keys.pressed.SPACE == false)
			{
				player.paused = false;
			}
			
			// this updates the tactical version of our hud, which is only active in the player's pause mode, makes available further information about the game's state
			// than is visible to the player normally
			hud.tacticalUpdate();
		}
	}	
}