package ;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.group.FlxTypedGroup;

/**
 * ...
 * @author Joseph Cox
 */
class Layers
{

	public static var PLAYERHUD:FlxGroup = new FlxGroup();
	public static var FILTERS:FlxTypedGroup<Filter> = new FlxTypedGroup<Filter>();
	public static var EFFECTS:FlxGroup = new FlxGroup();
	public static var DECALS:FlxGroup = new FlxGroup();
	public static var CREATURES:FlxGroup = new FlxGroup();
	public static var LOOT:FlxGroup = new FlxGroup();
	public static var FOREGROUND:FlxGroup = new FlxGroup();
	public static var UNDERGROUND:FlxGroup = new FlxGroup();
	public static var BACKGROUND:FlxGroup = new FlxGroup();
	#if mobile
	public static var BUTTONS:FlxGroup = new FlxGroup();
	#end
	
	public function new() 
	{
	}
	
}