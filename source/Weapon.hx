package ;

import flixel.FlxG;
import flash.geom.Point;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import flixel.util.FlxMath;
import flixel.FlxCamera;
import flixel.util.FlxRandom;

/**
 * ...
 * @author Joseph Cox
 */
class Weapon extends Equipment
{

	public static var FIST:String = "fist";
	public static var DAGGER:String = "dagger";
	public static var SWORD:String = "sword";
	public static var SPEAR:String = "spear";
	public static var AXE:String = "axe";
	public static var HAMMER:String = "hammer";
	
	public static var SLASH:String = "slash";
	public static var STAB:String = "stab";
	public static var CLEAVE:String = "cleave";
	public static var POUND:String = "pound";
	public static var STRIKE:String = "strike";
	
	public static var TYPE_DAGGER:Int = 0;
	public static var TYPE_SWORD:Int = 1;
	public static var TYPE_AXE:Int = 2;
	public static var TYPE_HAMMER:Int = 3;
	public static var TYPE_CLUB:Int = 4;
	public static var TYPE_SPEAR:Int = 5;
	
	public static var ATTACK_STAB:Int = 0;
	public static var ATTACK_SLASH:Int = 1;
	public static var ATTACK_CLEAVE:Int = 2;
	public static var ATTACK_STRIKE:Int = 3;
	public static var ATTACK_POUND:Int = 4;
	
	public static var EFFECT_STAB:Int = 0;
	public static var EFFECT_SLICE:Int = 1;
	public static var EFFECT_GASH:Int = 2;
	public static var EFFECT_CLEAVE:Int = 3;
	public static var EFFECT_DISABLE:Int = 4;
	public static var EFFECT_BREAK:Int = 5;
	public static var EFFECT_CAVE:Int = 6;
	public static var EFFECT_PENETRATE:Int = 7;
	
	private var l:Float;
	private var w:Float;
	private var t:Float;
	
	public var material:Material;
	
	public var fulllength:Float;
	
	public var idealrangeclose:Float;
	public var idealrangefar:Float;
	
	private var balance:Float;
	
	private var craftsmanship:Float;
	
	public var weight:Float;
	
	public var speed:Float;
	
	public var quality:Float;
	
	public var options:Array<String>;
	
	public var type:String;
	
	public function new(X:Int, Y:Int, Visual:String, lwt:Array<Float>, b:Float = .1, c:Float = .5, material:Material = null, type:String=null):Void 
	{
		super(X, Y, Visual);
		
		idealrangeclose = 0.0;
		idealrangeclose = 1.0;
		
		if (material == null)
		{
			material = Materials.iron;
		}
		this.material = material;
		
		if (type == null)
		{
			type = Weapon.SWORD;
		}
		this.type = type;
		
		l = lwt[0];
		w = lwt[1];
		t = lwt[2];
		
		weight = (l * w * t) * material.weightunit;
		
		var attacktype:Array<String>;
		if (type == DAGGER)
		{
			attacktype = [SLASH, SLASH, SLASH, STAB];
			
			fulllength = l * 1.1;
			
			idealrangeclose = 0.0;
			idealrangefar = 1.5;
		} else if (type == SWORD)
		{
			attacktype = [SLASH, SLASH, STAB];
			
			fulllength = l * 1.3;
			
			idealrangeclose = 0.4;
			idealrangefar = 1.2;
		} else if (type == AXE)
		{
			attacktype = [CLEAVE];
			
			fulllength = l * 8;
			
			idealrangeclose = 0.7;
			idealrangefar = 1.2;
		} else if (type == HAMMER)
		{
			attacktype = [STRIKE];
			
			fulllength = l * 15;
			
			idealrangeclose = 0.8;
			idealrangefar = 1.2;
		} else if (type == SPEAR)
		{
			attacktype = [STAB];
			
			fulllength = l * 10;
			weight *= 5;
			
			idealrangeclose = 0.7;
			idealrangefar = 1.2;
		} else if (type == FIST)
		{
			attacktype = [STRIKE];
			
			fulllength = l * 4;
			
			idealrangeclose = 0.0;
			idealrangefar = 1.0;
		} else
		{
			attacktype = [STAB, SLASH, CLEAVE, STRIKE, POUND];
			
			fulllength = 1;
		}
		options = attacktype;
		
		balance = b;
		
		speed = b / weight;
		
		craftsmanship = c;
		quality = c * material.qualityunit * balance;
		
		checkAllAttacks();
		
	}
	
	public function checkAllAttacks():Void
	{
		checkAttack(STAB);
		checkAttack(SLASH);
		checkAttack(CLEAVE);
		checkAttack(POUND);
		checkAttack(STRIKE);
	}
	
	public function checkAttack(type:String):Float
	{
		var damage:Float = 0.0;
		
		if (options.indexOf(type) != -1)
		{
			if (type == "slash")
			{
				damage = quality * 10 + weight * .1;
			} else if (type == "stab")
			{
				damage = quality * 11 - weight * .1;
			} else if (type == "cleave")
			{
				damage = quality * 11 + weight * .5;
			} else if (type == "pound")
			{
				damage = quality * 5 + weight * 1;
			} else if (type == "strike")
			{
				damage = quality * 10 + weight * .5;
			}
		}
		
		return damage;
	}
	
	public function checkStats():String
	{
		var string:String = Std.string("size: " + l +", " + w + ", " + t + "\n   ");
		string += "sub:" + balance + " bp, " + "\n   ";
		string += "MAIN: " + weight + " lbs, " + speed + " sp, " + quality + " qp";
		
		return string;
	}
	
	public static function randomOneHandSword(Visual:String="testsword", lwt:Array<Float> = null, b:Float = null, c:Float = null, material:Material = null):Weapon
	{
		if (b == null)
		{
			b = FlxRandom.floatRanged(.1, 1.0);
		}
		
		if (c == null)
		{
			c = FlxRandom.floatRanged(.1, .6);
		}
		
		if (lwt == null)
		{
			lwt = [FlxRandom.floatRanged(2.0, 3.0), FlxRandom.floatRanged(.17, .25), FlxRandom.floatRanged(.01, .02)];
		}
		
		if (material == null)
		{
			material = FlxRandom.getObject(Materials.materialsList, 2,Materials.materialsList.length);
			trace(material.name);
		}
		
		var type:String = Weapon.SWORD;
		
		var weapon:Weapon = new Weapon(0, 0, Visual, lwt, b, c, material, type);
		
		return weapon;
	}
}