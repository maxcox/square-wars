package ;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxRandom;
import haxe.ds.StringMap;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class WorldManager
{
	
	// the width and length of a region measured in full zones, regions are a zoomed out way of visualizing and organizing zones to allow macro-generation algorithms
	public static var regionSize:Int = 10;
	
	// the size of a region in pixels
	public static var regionSizePixels:Int = regionSize * MapManager.zoneSize * MapManager.mapTileSize;
	
	// the minimum and maximum bounds for the random "range" of generated world creatures, range determines how far the creatures wander from their home point
	public static var rangemin:Float = 2000;
	public static var rangemax:Float = 5000;
	
	// the minimum and maximum bounds for the random "leadership" of a generated leader, leadership*2 determines how many creatures will follow a leader
	public static var leadermin:Float = 1;
	public static var leadermax:Float = 20;
	
	// the amount of WorldCreatures allowed per region, this number will directly affect CPU performance as it determines loop lengths
	public static var creaturesPerRegion:Int = 1000;
	
	// offsets the region:zone relationship so that the first zone is in the center of the first region
	public static var regionOffset:Point = new Point( -regionSize / 2, -regionSize / 2);
	
	// a map of all the creatures being updated at the moment, separated by regions identified by a string formed from their coordinate
	// the group of the region the player occupies is always the first element in the array
	public var WorldCreatures:StringMap<FlxTypedGroup<WorldCreature>>;
	public var WorldLeaders:StringMap<FlxTypedGroup<WorldLeader>>;
	
	// string map that contains information telling us that region of the map, if the information is null we know the region requires generation,
	// a region is the largest chunk of the map that consists of (regionSize * regionSize) zones
	public var mapRegions:StringMap<RegionInfo>;
	
	public var creatureGenerator:WorldCreatureGenerator;
	
	private var genNeeded:Point;
	private var genWorldCreatures:FlxTypedGroup<WorldCreature>;
	
	private var updateRegions:Array<String>;
	
	public function new():Void
	{		
		// the object that holds functions for generating large numbers of creatures randomly
		creatureGenerator = new WorldCreatureGenerator();
		
		// initialize genNeeded
		genNeeded = null;
		
		// the four regions closest to the player
		updateRegions = new Array<String>();
		
		// initialize the mapRegions StringMap to avoid runtime error
		mapRegions = new StringMap<RegionInfo>();
		
		// initialize WorldCreatures array of lists to avoid runtime error
		WorldCreatures = new StringMap<FlxTypedGroup<WorldCreature>>();
		
		// initialize WorldLeaders array of lists to avoid runtime error
		WorldLeaders = new StringMap<FlxTypedGroup<WorldLeader>>();
	}
	
	public function generateFirst():Void
	{
		generateRegion(new Point(0, 0));
	}
	
	private function generateRegion(regionToGenerate:Point):Void
	{
		//trace("generate");
		//trace(regionToGenerate);
		var regionID:String = Std.string(regionToGenerate.x + "," + regionToGenerate.y);
		
		var region:RegionInfo = new RegionInfo();
		region.randomize();
		
		mapRegions.set(regionID, region);
		
		var zoneX:Int = Std.int(regionToGenerate.x * regionSize);
		var zoneY:Int = Std.int(regionToGenerate.y * regionSize);
		
		var tileX:Int = Std.int((zoneX + WorldManager.regionOffset.x) * MapManager.zoneSize);
		var tileY:Int = Std.int((zoneY + WorldManager.regionOffset.y) * MapManager.zoneSize);
		
		var minimumPixelsX:Int = Std.int((tileX + MapManager.mapOffset.x) * MapManager.mapTileSize);
		var minimumPixelsY:Int = Std.int((tileY + MapManager.mapOffset.y) * MapManager.mapTileSize);
		
		var generatedCreatures:FlxTypedGroup<WorldCreature> = creatureGenerator.randomCreatures(minimumPixelsX, minimumPixelsY, creaturesPerRegion);
		var generatedLeaders:FlxTypedGroup<WorldLeader> = new FlxTypedGroup<WorldLeader>();
		
		function separateLeaders(creature:WorldCreature):Void
		{
			if (Std.is(creature, WorldLeader) || creature.leader == null)
			{
				generatedCreatures.remove(creature);
				
				generatedLeaders.add(cast creature);
			}
		}
		
		generatedCreatures.forEach(separateLeaders);
		
		WorldCreatures.set(regionID, generatedCreatures);
		WorldLeaders.set(regionID, generatedLeaders);
		
		genNeeded = null;
	}
	
	private function asyncGenerateRegion(regionToGenerate:Point):Void
	{
		if (creatureGenerator.genProgress == 0)
		{
			//trace("generate");
			//trace(regionToGenerate);
			var regionID:String = Std.string(regionToGenerate.x + "," + regionToGenerate.y);
			
			var region:RegionInfo = new RegionInfo();
			region.randomize();
			
			mapRegions.set(regionID, region);
			
			genWorldCreatures = new FlxTypedGroup<WorldCreature>();
		}
		
		var zoneX:Int = Std.int(regionToGenerate.x * regionSize);
		var zoneY:Int = Std.int(regionToGenerate.y * regionSize);
		
		var tileX:Int = Std.int((zoneX + WorldManager.regionOffset.x) * MapManager.zoneSize);
		var tileY:Int = Std.int((zoneY + WorldManager.regionOffset.y) * MapManager.zoneSize);
		
		var minimumPixelsX:Int = Std.int((tileX + MapManager.mapOffset.x) * MapManager.mapTileSize);
		var minimumPixelsY:Int = Std.int((tileY + MapManager.mapOffset.y) * MapManager.mapTileSize);
		
		creatureGenerator.asyncRandomCreatures(minimumPixelsX, minimumPixelsY, creaturesPerRegion, genWorldCreatures);
		
		if (creatureGenerator.genProgress >= creaturesPerRegion)
		{
			var regionID:String = Std.string(regionToGenerate.x + "," + regionToGenerate.y);
			
			creatureGenerator.genProgress = 0;
			
			genNeeded = null;
			
			//WorldCreatures.set(Std.string(regionToGenerate.x + "," + regionToGenerate.y), genWorldCreatures);
			var generatedCreatures:FlxTypedGroup<WorldCreature> = genWorldCreatures;
			var generatedLeaders:FlxTypedGroup<WorldLeader> = new FlxTypedGroup<WorldLeader>();
			
			function separateLeaders(creature:WorldCreature):Void
			{
				if (Std.is(creature, WorldLeader) || creature.leader == null)
				{
					generatedCreatures.remove(creature);
					
					generatedLeaders.add(cast creature);
				}
			}
			
			generatedCreatures.forEach(separateLeaders);
			
			WorldCreatures.set(regionID, generatedCreatures);
			WorldLeaders.set(regionID, generatedLeaders);
			
			genWorldCreatures = null;
		}
	}
		
	private function checkRegions(player:Player):Void
	{
		// sets to px and py the coordinate values of the left most corner of the tile the player occupies
		var px:Float = Math.floor(player.x);
		var py:Float = Math.floor(player.y);
		
		// finds the coordinates in tiles of the tile the player occupies and sets to playertilex and playertiley
		var playertilex:Int = Std.int(px / MapManager.mapTileSize - MapManager.mapOffset.x);
		var playertiley:Int = Std.int(py / MapManager.mapTileSize - MapManager.mapOffset.y);
		
		// finds the coordinates in zones of the zone the player occupies and sets to playerzonex and playerzoney
		var playerzonex:Int = Math.floor(playertilex / MapManager.zoneSize - regionOffset.x);
		var playerzoney:Int = Math.floor(playertiley / MapManager.zoneSize - regionOffset.y);
		
		// half the size of a region
		var regionhalfsize:Int = Std.int(regionSize / 2);
		
		// finds the player's position compared to the halfway lines of the region, horizontally and vertically
		var playeroffx:Int = FlxMath.signOf(playerzonex % regionSize - regionhalfsize);
		var playeroffy:Int = FlxMath.signOf(playerzoney % regionSize - regionhalfsize);
		
		// finds the coordinates in regions of the region the player occupies and sets to playerregionx and playerregiony
		var playerregionx:Int = Math.floor(playerzonex / regionSize);
		var playerregiony:Int = Math.floor(playerzoney / regionSize);
		
		var horinext:Int = playerregionx + playeroffx;
		var vertnext:Int = playerregiony + playeroffy;
		
		if (mapRegions.exists(Std.string(playerregionx + "," + playerregiony)) == false)
		{
			creatureGenerator.genProgress = 0;
			genNeeded = new Point(playerregionx, playerregiony);
		} else if (mapRegions.exists(Std.string(horinext + "," + playerregiony)) == false)
		{
			creatureGenerator.genProgress = 0;
			genNeeded = new Point(horinext, playerregiony);
		} else if (mapRegions.exists(Std.string(playerregionx + "," + vertnext)) == false)
		{
			creatureGenerator.genProgress = 0;
			genNeeded = new Point(playerregionx, vertnext);
		} else if (mapRegions.exists(Std.string(horinext + "," + vertnext)) == false)
		{
			creatureGenerator.genProgress = 0;
			genNeeded = new Point(horinext, vertnext);
		}
		
		updateRegions = [Std.string(playerregionx + "," + playerregiony), Std.string(horinext + "," + playerregiony), Std.string(playerregionx + "," + vertnext), Std.string(horinext + "," + vertnext)];
	}
	
	private function getRegion(checkPoint:Point):Point
	{
		var region:Point;
		
		// sets to px and py the coordinate values of the left most corner of the tile the point occupies
		var px:Float = Math.floor(checkPoint.x);
		var py:Float = Math.floor(checkPoint.y);
		
		// finds the coordinates in tiles of the tile the point occupies and sets to playertilex and playertiley
		var pointtilex:Int = Std.int(px / MapManager.mapTileSize - MapManager.mapOffset.x);
		var pointtiley:Int = Std.int(py / MapManager.mapTileSize - MapManager.mapOffset.y);
		
		// finds the coordinates in zones of the zone the point occupies and sets to playerzonex and playerzoney
		var pointzonex:Int = Math.floor(pointtilex / MapManager.zoneSize - regionOffset.x);
		var pointzoney:Int = Math.floor(pointtiley / MapManager.zoneSize - regionOffset.y);
		
		// finds the coordinates in regions of the region the point occupies and sets to playerregionx and playerregiony
		var pointregionx:Int = Math.floor(pointzonex / regionSize);
		var pointregiony:Int = Math.floor(pointzoney / regionSize);
		
		region = new Point(pointregionx, pointregiony);
		
		return region;
	}
	
	private function creaturesUpdate(player:Player):Void
	{
		var currentRegion:Point = getRegion(new Point(player.x, player.y));
		
		var distancex:Float = FlxG.width * 2;
		var distancey:Float = FlxG.height * 2;
		
		var minx:Float = player.x - distancex;
		var miny:Float = player.y - distancey;
		
		var maxx:Float = player.x + distancex;
		var maxy:Float = player.y + distancey;
		
		function checkForRender(creature:WorldCreature):Void
		{
			if (creature.x > minx && creature.y > miny)
			{
				// THIS HAS PROBLEMS PLEASE FIX THIS
				if (creature.x < maxx && creature.y < maxy && creature.rendered == false)
				{
					creature.sprite = new FlxSprite(creature.x, creature.y);
					creature.sprite.makeGraphic(16, 16, FlxColor.RED);
					
					if (Std.is(creature, WorldLeader) == true)
					{
						creature.sprite.makeGraphic(32, 32, FlxColor.BLUE);
					}
					
					Layers.DECALS.add(creature.sprite);
					
					creature.rendered = true;
				} else if (creature.rendered == true)
				{
					Layers.DECALS.remove(creature.sprite);
					
					creature.rendered = false;
				}
			} else if (creature.rendered == true)
			{
				Layers.DECALS.remove(creature.sprite);
				
				creature.rendered = false;
			}
			
			if (creature.leader == null && Std.is(creature, WorldLeader) == false)
			{
				if (creature.homex > minx && creature.homey > miny)
				{
					if (creature.homex < maxx && creature.homey < maxy)
					{
						Layers.DECALS.add(creature.home);
					}
				}
			}
		}
		
		var diffx:Float;
		var diffy:Float;
		
		var counter:Int = 0;
		
		function moveAround(creature:WorldLeader):Void
		{
			if (creature.rendered == false)
			{
				creature.x += FlxRandom.intRanged( -10, 10);
				creature.y += FlxRandom.intRanged( -10, 10);
				
				/*
				if (creature.leader != null)
				{
					diffx = creature.x - creature.leader.x;
					diffy = creature.y - creature.leader.y;
				} else if (Std.is(creature, WorldLeader) == false)
				{
					diffx = creature.x - creature.homex;
					diffy = creature.y - creature.homey;
				} else
				{
					diffx = 0;
					diffy = 0;
				}
				
				if (Math.abs(diffx) > creature.range)
				{
					creature.x -= FlxMath.signOf(diffx) * 20;
				}
				
				if (Math.abs(diffy) > creature.range)
				{
					creature.y -= FlxMath.signOf(diffy) * 20;
				}
				
				if (creature.sprite != null)
				{
					creature.sprite.x = creature.x;
					creature.sprite.y = creature.y;
				}
				*/
			} else
			{
				creature.x = Std.int(creature.sprite.x);
				creature.y = Std.int(creature.sprite.y);
			}
			
			counter ++;
		}
		
		for (c in WorldCreatures.keys())
		{
			var creatures:FlxTypedGroup<WorldCreature> = cast(WorldCreatures.get(c));
				
			if (Std.string(currentRegion.x + "," + currentRegion.y) == c)
			{
				//creatures.forEach(checkForRender);
			}
		}
		
		for (l in WorldLeaders.keys())
		{
			var leaders:FlxTypedGroup<WorldLeader> = cast(WorldLeaders.get(l));
			
			if (updateRegions.indexOf(l) != -1)
			{
				leaders.forEach(moveAround);
			}
		}
		
		trace(counter);
		
		
	}
	
	public function update(player:Player):Void
	{
		if (genNeeded != null)
		{
			asyncGenerateRegion(genNeeded);
		} else
		{
			checkRegions(player);
		}
		
		
		//var elapsedTime:Int = flash.Lib.getTimer();
		//creaturesUpdate(player);
		//trace(flash.Lib.getTimer() - elapsedTime);
	}
}