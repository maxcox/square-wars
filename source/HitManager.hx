package ;
import flixel.FlxBasic;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxCollision;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class HitManager
{
	
	public static var rangescale:Float = 10;
	
	public function new() 
	{
		
	}
	
	public static function HitAnythingTest(creature:Creature, weapon:Weapon, attacktype:String="slash", attackheight:String="high"):Void
	{
		var hitCreatures:Array<Creature> = new Array<Creature>();
		var effectiveArray:Array<Float> = new Array <Float>();
		
		var base:Float;
		if (attacktype == "slash")
		{
			base = creature.baseRange * .7;
		} else if (attacktype == "strike")
		{
			base = creature.baseRange * .6;
		} else
		{
			base = creature.baseRange;
		}
		
		var range:Float = (base + weapon.fulllength) * rangescale;
		
		var attacktypeold:String = attacktype;
		
		if (attacktype == "cleave")
		{
			attacktype = "slash";
		}
		
		var attack:Effect = new Effect(Std.int(creature.x - creature.width/2), Std.int(creature.y - creature.height/2), 32, 32+range, Std.string("assets/images/effects/" + attacktype + "attack.png"), creature);
		Layers.EFFECTS.add(attack);
		
		attacktype = attacktypeold;
		
		var hitbox:FlxSprite = new FlxSprite(0,0);
		function checkRadius(basic:FlxBasic):Void
		{
			var creatureToCheck:Creature = cast basic;
			if (creature != creatureToCheck)
			{
				var distancex:Float = creature.x - creatureToCheck.x;
				var distancey:Float = creature.y - creatureToCheck.y;
				var distance:Float = Math.sqrt(Math.pow(distancex, 2) + Math.pow(distancey, 2));
				
				var distanceangle:Float = Math.atan2(distancey, distancex) * 180 / Math.PI;
				var creatureangle:Float = creature.angle - 90;
				
				if (distance - creatureToCheck.diagonal <= range)
				{
					hitbox.setPosition(creature.x + creature.width / 2, creature.y + creature.height / 2);
					var hitboxlength:Float = 0;
					
					if (attacktype == "slash" || attacktype == "strike" || attacktype == "cleave")
					{
						hitboxlength = creature.torso.width;
					} else if (attacktype == "stab")
					{
						hitboxlength = .2*rangescale;
					} else
					{
						hitboxlength = Std.int(range);
					}
					hitbox.makeGraphic(Std.int(range), Std.int(hitboxlength), FlxColor.BLACK);
					hitbox.alpha = .1;
					hitbox.x -= hitbox.width / 2;
					hitbox.y -= hitbox.height / 2;
					Layers.DECALS.add(hitbox);
					hitbox.angle = creatureangle;
					hitbox.x += Math.cos(hitbox.angle * Math.PI / 180) * hitbox.width / 2;
					hitbox.y += Math.sin(hitbox.angle * Math.PI / 180) * hitbox.width / 2;
					
					if (FlxCollision.pixelPerfectCheck(hitbox, creatureToCheck.torso) == true)
					{
						var mdx:Float = (creature.x + creature.width / 2) - (creatureToCheck.x + creatureToCheck.width / 2);
						var mdy:Float = (creature.y + creature.height / 2) - (creatureToCheck.y + creatureToCheck.height / 2);
						var middistance:Float = Math.sqrt(mdx * mdx + mdy * mdy);
						
						trace(middistance);
						
						var effectiveness:Float = 0;
						var rangeratio:Float = middistance / range;
						trace(rangeratio);
						
						if (rangeratio > weapon.idealrangeclose && rangeratio < weapon.idealrangefar)
						{
							var idealwidth:Float = (weapon.idealrangefar - weapon.idealrangeclose);
							var midideal:Float = idealwidth / 2 + weapon.idealrangeclose;
							trace("MIDIDEAL: " + midideal);
							
							var ideal:FlxSprite = new FlxSprite(creature.x + creature.width/2, creature.y + creature.height/2);
							ideal.makeGraphic(2, 2, FlxColor.RED);
							ideal.x += Math.cos((hitbox.angle) * Math.PI / 180) * midideal*range;
							ideal.y += Math.sin((hitbox.angle) * Math.PI / 180) * midideal*range;
							Layers.EFFECTS.add(ideal);
							
							effectiveness = Math.abs(rangeratio - midideal) / (idealwidth / 2);
						}
						
						effectiveArray.push(effectiveness);
						
						hitCreatures.push(creatureToCheck);
					}
				}
				
			}
		}
		
		CreatureManager.ActiveCreatures.forEachAlive(checkRadius);
		//hitbox.destroy();
		
		if (hitCreatures.length != 0)
		{
			for (i in 0...hitCreatures.length)
			{
				Battle.WeaponAttack(creature, hitCreatures[i], attacktype, effectiveArray[i], attackheight);
			}
		}
	}
	
}