package ;
import flixel.util.FlxRandom;
import haxe.ds.StringMap;

/**
 * ...
 * @author Joseph Cox
 */
class SpeechManager
{
	
	public static var nouns:Array<String> = ["time", "issue", "year", "side", "person", "kind", "way", "head", "day", "house", "man", "service", "thing", "friend", "woman", "father", "life", "power", "child", "hour", "world", "game", "school", "line", "state", "end", "family", "member", "student", "law", "group", "car", "country", "city", "problem", "community", "hand", "name", "part", "president", "place", "team", "case", "minute", "week", "idea", "company", "kid", "system", "body", "program", "information", "question", "back", "work", "parent", "government", "face", "number", "others", "night", "level", "Mr", "office", "point", "door", "home", "health", "water", "person", "room", "art", "mother", "war", "area", "history", "money", "party", "storey", "result", "fact", "change", "month", "morning", "lot", "reason", "right", "research", "study", "girl", "book", "guy", "eye", "food", "job", "moment", "word", "air", "business", "teacher"];
	public static var pronouns:Array<String> = ["I", "me", "you", "she", "he", "it", "her", "him", "they", "them"];
	public static var verbs:Array<String> = ["be", "have", "do", "say", "go", "get", "make", "know", "think", "take", "see", "come", "want", "use", "find", "give", "tell", "work", "call", "try", "ask", "need", "feel", "become", "leave", "put", "mean", "keep", "let", "begin", "seem", "help", "show", "hear", "play", "run", "move", "live", "believe", "bring", "happen", "write", "sit", "stand", "lose", "pay", "meet", "include", "continue", "set", "learn", "change", "lead", "understand", "watch", "follow", "stop", "create", "speak", "read", "spend", "grow", "open", "walk", "win", "teach", "offer", "remember", "consider", "appear", "buy", "serve", "die", "send", "build", "stay", "fall", "cut", "reach", "kill", "raise", "pass", "sell", "decide", "return", "explain", "hope", "develop", "carry", "break", "receive", "agree", "support", "hit", "produce", "eat", "cover", "catch", "draw", "choose"];
	public static var adjectives:Array<String> = ["other", "new", "good", "high", "old", "great", "big", "small", "large", "national", "young", "different", "black", "long", "little", "important", "political", "bad", "white", "real", "best", "right", "social", "only", "public", "sure", "low", "early", "able", "human", "local", "late", "hard", "major", "better", "economic", "strong", "possible", "whole", "free", "military", "true", "federal", "international", "full", "special", "easy", "clear", "recent", "certain", "personal", "open", "red", "difficult", "available", "likely", "short", "single", "medical", "current", "wrong", "private", "past", "foreign", "fine", "common", "poor", "natural", "significant", "similar", "hot", "dead", "central", "happy", "serious", "ready", "simple", "left", "physical", "general", "environmental", "financial", "blue", "democratic", "dark", "various", "entire", "close", "legal", "religious", "cold", "final", "main", "green", "nice", "huge", "popular", "traditional", "cultural"];
	
	
	
	public static var greetings:Array<String> = ["Hello", "Hi", "Hey", "Greetings", "Salutations", "Howdy"];
	
	public static var SpeechPossibilities:StringMap<String> = new StringMap<String>();
	
	public function new() 
	{
		
	}
	
	public static function Sentence():String
	{
		var person:ConceptObject = new ConceptObject("Felicia", true, "female", ["gay", "fun", "Asian"]);
		
		var subject:String = FlxRandom.getObject(nouns);
		var verb:String = FlxRandom.getObject(verbs);
		var adjective:String = FlxRandom.getObject(adjectives);
		var object:String = FlxRandom.getObject(nouns);
		
		var sentence:String = "The " + subject + " " + verb + "s the " + adjective + " " + object + ".";
		
		return sentence;
	}
	
	public static function Describe(object:ConceptObject):String
	{
		var sentence:String = object.name + " is " + FlxRandom.getObject(object.properties) + ".";
		
		return sentence;
	}
	
	public static function Speak(creature1:Creature, creature2:Creature):String
	{
		var speech:String = "";
		
		if (creature1.checkEvent("MET_" + creature1.name.toUpperCase()) == false)
		{
			
		}
		
		return speech;
	}
	
	public function fillPossibilities():Void
	{
		SpeechPossibilities.set("HELLO_UNIVERSAL", "Hi:Hello:Hey:Greetings:Salutations");
		SpeechPossibilities.set("HELLO_MORNING", "Good Morning:Morning");
		SpeechPossibilities.set("HELLO_AFTERNOON", "Good Afternoon:Afternoon");
		SpeechPossibilities.set("HELLO_EVENING", "Good Evening:Evening");
	}
	
}