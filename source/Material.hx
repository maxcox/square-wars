package;

/**
 * ...
 * @author ...
 */
class Material 
{

	public var name:String;
	public var weightunit:Float;
	public var qualityunit:Float;
	
	public function new(name:String, wu:Float, qu:Float):Void
	{
		this.name = name;
		weightunit = wu;
		qualityunit = qu;
	}
	
}