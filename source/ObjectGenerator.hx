package ;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxRandom;
import flixel.util.FlxRect;
import openfl.Assets;

/**
 * ...
 * @author Joseph Cox
 */
class ObjectGenerator
{

	public function new() :Void
	{
		
	}
	
	public function generateObjects(boundsRect:FlxRect, biome:String="grass"):FlxTypedGroup<MapObject>
	{
		var objects:FlxTypedGroup<MapObject> = new FlxTypedGroup<MapObject>();
		
		var abundance:Int = 1000;
		
		var newx:Int;
		var newy:Int;
		var newobject:MapObject;
		
		for (i in 0...abundance)
		{
			newx = FlxRandom.intRanged(Std.int(boundsRect.x), Std.int(boundsRect.x + boundsRect.width));
			newy = FlxRandom.intRanged(Std.int(boundsRect.y), Std.int(boundsRect.y + boundsRect.height));
			
			var rand:Float = FlxRandom.intRanged(0, 10);
			var id:String = "";
			
			if (rand == 0)
			{
				id = "grass/objects/rocks/plainrock";
			} else if (rand < 2)
			{
				id = "grass/objects/flora/poplar";
			} else
			{
				id = "grass/objects/flora/bushy";
			}
			
			newobject = new MapObject(newx, newy, FlxRandom.intRanged(0, 360), FlxRandom.floatRanged(.6,1.0), id);
			
			if (Assets.getText("assets/data/" + id + ".txt") != null)
			{
				var info:String = Assets.getText("assets/data/" + id + ".txt");
				var infoArray:Array<String> = info.split(":");
				
				newobject.hitsize = Std.parseFloat(infoArray[1]);
				newobject.z = Std.parseInt(infoArray[2]);
			}
			
			objects.add(newobject);
		}
		
		return objects;
	}
	
}