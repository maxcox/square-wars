package ;

import flixel.FlxBasic;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxRandom;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class MapObject extends FlxBasic
{

	// the object's current coordinates in pixels
	public var x:Int;
	public var y:Int;
	public var z:Int;
	
	public var rot:Int;
	
	public var scale:Float;
	
	public var hitsize:Float;
	
	public var id:String;
	
	public var sprite:FlxSprite;
	
	public var rendered:Bool;
		
	public function new(x:Int, y:Int, rot:Int, scale:Float=1.0, id:String=""):Void
	{
		super();
		
		// set the object's coordinates
		this.x = x;
		this.y = y;
		
		z = 0;
		hitsize = 1.0;
		
		this.rot = rot;
		
		this.scale = scale;
		
		this.id = id;
		
		rendered = false;
	}
	
}