package body;
import haxe.ds.HashMap;
import haxe.ds.ObjectMap;
import haxe.ds.StringMap;

/**
 * ...
 * @author Joseph Cox
 */
class Body
{

	public var parts:StringMap<BodyPart>;
	
	public var totalsize:Float;
	
	public var disabled:StringMap<Muscle>;
	public var disabledCount:StringMap<Int>;
	public var disableMax:Int;
	
	public var checkflag:Bool;
	
	public function new(_parts:Array<BodyPart>):Void 
	{
		checkflag = false;
		
		totalsize = 0;
		
		if (_parts != null)
		{
			parts = new StringMap<BodyPart>();
			
			for (i in _parts)
			{
				var part:BodyPart = cast i;
				
				if (parts.exists(part.name))
				{
					var rightpart:BodyPart = parts.get(part.name);
					parts.remove(part.name);
					rightpart.name = "Right " + rightpart.name;
					parts.set(rightpart.name, rightpart);
					for (m in rightpart.muscles)
					{
						m.name = "Right " + m.name;
					}
					
					part.name = "Left " + part.name;
					for (m in part.muscles)
					{
						m.name = "Left " + m.name;
					}
				}
				parts.set(part.name, part);
				
				totalsize += part.size;
			}
		}
		
		disabled = new StringMap<Muscle>();
		disabledCount = new StringMap<Int>();
		
		disableMax = 100;
	}
	
	public function setDisabled(muscle:Muscle):Void
	{
		if (disabled.exists(muscle.name) == false)
		{
			disabled.set(muscle.name, muscle);
			disabledCount.set(muscle.name, 0);
		}
	}
	
	public function update():Void
	{
		for (k in disabledCount.keys())
		{
			var count:Int = disabledCount.get(k);
			if (count < disableMax)
			{
				disabledCount.set(k, count + 1);
			} else
			{
				var part:Muscle = disabled.get(k);
				
				part.disable --;
				
				if (part.disable > 0)
				{
					disabledCount.set(k, 0);
				} else
				{
					disabled.remove(k);
					disabledCount.remove(k);
					
					checkflag = true;
				}
			}
		}
	}
	
}