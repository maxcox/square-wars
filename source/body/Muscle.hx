package body;

/**
 * ...
 * @author Joseph Cox
 */
class Muscle
{

	public var health:Float;
	public var maxhealth:Float;
	
	public var bleeding:Float;
	
	public var name:String;
	
	public var size:Float;
	
	public var sheltered:Bool;
	
	public var disable:Int;
	public var broke:Int;
	public var cleave:Int;
	public var gash:Int;
	public var stab:Int;
	
	public function new(name:String, maxhealth:Float, size:Float = 1, sheltered:Bool=false):Void 
	{
		this.name = name;
		
		this.maxhealth = maxhealth;
		health = maxhealth;
		
		bleeding = 0;
		
		this.size = size;
		
		this.sheltered = sheltered;
		
		disable = broke = cleave = gash = stab = 0;
	}
	
}