package body;
import haxe.ds.ObjectMap;
import haxe.ds.StringMap;

/**
 * ...
 * @author Joseph Cox
 */
class Bodies
{

	//THIS IS A STATIC CLASS WITH PRESET BODIES
	
	//BODY PARTS
	
	public static var HEAD:BodyPart = new BodyPart("Head", [new Muscle("Brain", 1, 2), new Muscle("Jaw", 3, 2), new Muscle("Neck", 2, 2), new Muscle("Right Eye", 1, 1), new Muscle("Left Eye", 1, 1), new Muscle("Nose",1,1), new Muscle("Right Ear", 1, 1), new Muscle("Left Ear", 1, 1)]);
	public static var UPPERTORSO:BodyPart = new BodyPart("Upper Torso", [new Muscle("Heart", 1, 1, true), new Muscle("Right Lung", 1, 2, true), new Muscle("Left Lung", 1, 2, true), new Muscle("Stomach", 1, 1, true), new Muscle("Right Pectoral", 5, 5), new Muscle("Left Pectoral", 5, 5), new Muscle("Right Oblique", 5, 6), new Muscle("Left Oblique", 5, 6), new Muscle("Abdomen", 5, 6), new Muscle("Back", 5, 8), new Muscle("Spine", 5, 2, true)]);
	public static var UPPERARM:BodyPart = new BodyPart("Upper Arm", [new Muscle("Deltoid", 5, 5), new Muscle("Bicep", 5, 2), new Muscle("Tricep", 5, 3)]);
	public static var LOWERARM:BodyPart = new BodyPart("Lower Arm", [new Muscle("Forearm", 5, 9), new Muscle("Palm", 5, 3), new Muscle("Thumb", 1, 1), new Muscle("Index Finger", 1, .5), new Muscle("Middle Finger", 1, .5), new Muscle("Ring Finger", 1, .5), new Muscle("Pinky Finger", 1, .5)]);
	public static var LOWERTORSO:BodyPart = new BodyPart("Lower Torso", [new Muscle("Groin", 5, 3), new Muscle("Gluteus", 5, 15)]);
	public static var UPPERLEG:BodyPart = new BodyPart("Upper Leg", [new Muscle("Inner Thigh", 5, 3), new Muscle("Outer Thigh", 5, 10), new Muscle("Hamstring", 5, 6), new Muscle("Knee", 5, 4)]);
	public static var LOWERLEG:BodyPart = new BodyPart("Lower Leg", [new Muscle("Calf", 5, 9), new Muscle("Shin", 5, 4), new Muscle("Ankle", 5, 4), new Muscle("Foot", 5, 3)]);
	
	public function new():Void
	{
		
	}
	
	private static function CopyBodyPart(part:BodyPart):BodyPart
	{
		var copyMuscles:Array<Muscle> = new Array<Muscle>();
		for (i in part.muscles)
		{
			copyMuscles.push(new Muscle(i.name, i.maxhealth, i.size, i.sheltered));
		}
		var newPart:BodyPart = new BodyPart(part.name, copyMuscles);
		
		return newPart;
	}
	
	public static function ReturnHumanBody():Body
	{
		var HumanBody:Body = new Body([CopyBodyPart(HEAD), CopyBodyPart(UPPERTORSO), CopyBodyPart(UPPERARM), CopyBodyPart(UPPERARM), CopyBodyPart(LOWERARM), CopyBodyPart(LOWERARM), CopyBodyPart(LOWERTORSO), CopyBodyPart(UPPERLEG), CopyBodyPart(UPPERLEG), CopyBodyPart(LOWERLEG), CopyBodyPart(LOWERLEG)]);
		
		return HumanBody; 
	}
}