package body;
import haxe.ds.StringMap;

/**
 * ...
 * @author Joseph Cox
 */
class BodyPart
{

	public var name:String;
	
	public var muscles:StringMap<Muscle>;
	
	public var size:Float;
	
	public var exteriorsize:Float;
	
	public var cuts:Int;
	
	public function new(name, muscles:Array<Muscle>):Void
	{
		cuts = 0;
		
		size = 0;
		exteriorsize = 0;
		
		this.name = name;
		
		this.muscles = new StringMap<Muscle>();
		for (i in muscles)
		{
			var muscle:Muscle = cast(i, Muscle);
			
			this.muscles.set(i.name, i);
			
			size += muscle.size;
			
			if (muscle.sheltered == false)
			{
				exteriorsize += muscle.size;
			}
		}
	}
	
}