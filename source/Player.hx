package ;

import flixel.addons.ui.FlxButtonPlus;
import flixel.FlxG;
import flash.geom.Point;
import flixel.group.FlxGroup;
import flixel.ui.FlxButton;
import flixel.util.FlxPoint;
import flixel.util.FlxMath;
import flixel.FlxCamera;

/**
 * ...
 * @author Joseph Cox
 */
class Player extends Creature
{

	#if mobile
		
	public var leftbutton:FlxButton;
	public var rightbutton:FlxButton;
	public var upbutton:FlxButton;
	public var downbutton:FlxButton;
		
	#end
	
	public var paused:Bool;
	
	public var attackpaused:Bool;
	
	public function new(X:Int, Y:Int, Visual:String) 
	{
		super(X, Y, Visual);
		
		attackpaused = false;
		paused = false;
		
		#if mobile
		
		leftbutton = new FlxButton(5, FlxG.height - 40, "LEFT");
		rightbutton = new FlxButton(85, FlxG.height - 40, "RIGHT");
		upbutton = new FlxButton(45, FlxG.height - 60, "UP");
		downbutton = new FlxButton(45, FlxG.height - 20, "DOWN");
		
		Layers.BUTTONS.add(leftbutton);
		Layers.BUTTONS.add(rightbutton);
		Layers.BUTTONS.add(upbutton);
		Layers.BUTTONS.add(downbutton);
		
		#end
	}
	
	override public function update():Void
	{
		super.update();
		
		#if !mobile
		if (FlxG.keys.pressed.A)
		{
			movement.x = -1;
		} else if (FlxG.keys.pressed.D)
		{
			movement.x = 1;
		} else
		{
			movement.x = 0;
		}
		
		if (FlxG.keys.pressed.W)
		{
			movement.y = -1;
		} else if (FlxG.keys.pressed.S)
		{
			movement.y = 1;
		} else
		{
			movement.y = 0;
		}
		
		if (FlxG.keys.justPressed.K)
		{
			if (equipment.exists("weapon"))
			{
				unequip("weapon");
			} else
			{
				var weapon:Weapon = new Weapon(0, 0, "testsword", [2.0/*FlxRandom.floatRanged(2.0, 3.0)*/, .17/*FlxRandom.floatRanged(.17, .25)*/, .0125/*FlxRandom.floatRanged(.01, .02)*/], .5, .5, Materials.iron);
				equip(weapon);
			}
		}
		
		if (FlxG.keys.justPressed.H)
		{
			if (equipment.exists("weapon"))
			{
				unequip("weapon");
			} else
			{
				var dagger:Weapon = new Weapon(0, 0, "testsword", [0.5, .08, .01], .5, .5, Materials.iron, Weapon.DAGGER);
				var axe:Weapon = new Weapon(0, 0, "testsword", [.33, .46, .125], .5, .5, Materials.iron, Weapon.AXE);
				var hammer:Weapon = new Weapon(0, 0, "testsword", [.16, .42, .16], .5, .5, Materials.iron, Weapon.HAMMER);
				equip(hammer);
			}
		}
		
		if (FlxG.keys.justPressed.Y)
		{
			if (equipment.exists("weapon"))
			{
				unequip("weapon");
			} else
			{
				var dagger:Weapon = new Weapon(0, 0, "testsword", [0.5, .08, .01], .5, .5, Materials.iron, Weapon.DAGGER);
				equip(dagger);
			}
		}
		
		if (FlxG.keys.justPressed.G)
		{
			if (equipment.exists("weapon"))
			{
				unequip("weapon");
			} else
			{
				var spear:Weapon = new Weapon(0, 0, "testsword", [0.6, .125, .02], .5, .5, Materials.iron, Weapon.SPEAR);
				equip(spear);
			}
		}
		
		if (FlxG.keys.justPressed.J)
		{
			if (equipment.exists("torso"))
			{
				unequip("torso");
			} else
			{
				var armor:Armor = new Armor(0,0, "testarmor", "torso");
				equip(armor);
			}
		}
		
		if (FlxG.keys.justPressed.L)
		{
			if (equipment.exists("legs"))
			{
				unequip("legs");
			} else
			{
				var leggings:Armor = new Armor(0, 0, "testpants", "legs");
				equip(leggings);
			}
		}
		
		if (FlxG.keys.pressed.SHIFT && oxygen > 2)
		{
			oxygen -= .05;//.20;
			currentSpeed = SPEED * 2;
			running = true;
		} else
		{
			currentSpeed = SPEED;
			running = false;
		}
		
		if (FlxG.mouse.justPressed)
		{
			attack("high");
		} else if (FlxG.mouse.justPressedRight)
		{
			attack("low");
		}
		
		if (FlxG.keys.pressed.SPACE == true)
		{
			paused = true;
		}
		
		#else
		
		if (leftbutton.status == FlxButton.PRESSED)
		{
			movement.x = -1;
		} else if (rightbutton.status == FlxButton.PRESSED)
		{
			movement.x = 1;
		} else
		{
			movement.x = 0;
		}
		
		if (upbutton.status == FlxButton.PRESSED)
		{
			movement.y = -1;
		} else if (downbutton.status == FlxButton.PRESSED)
		{
			movement.y = 1;
		} else
		{
			movement.y = 0;
		}
		
		#end

		var mousePosition:FlxPoint = FlxG.mouse.getWorldPosition(FlxG.camera);

		var mousediffx:Float = mousePosition.x - (x + width / 2);
		var mousediffy:Float = mousePosition.y - (y + height / 2);

		if (mousediffx != 0)
		{
			direction.x = mousediffx;
		} else
		{
			direction.x = 0;
		}

		if (mousediffy != 0)
		{
			direction.y = mousediffy;
		} else
		{
			direction.y = 0;
		}
		
	}
	
}