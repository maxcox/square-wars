package ;
import body.Body;
import body.BodyPart;
import body.Muscle;
import flixel.util.FlxRandom;

/**
 * ...
 * @author Joseph Cox
 */
class Capabilities
{

	public function new() 
	{
		
	}
	
	public static function ApplyEffectsAfterDamage(creature:Creature):Void
	{
		var body:Body = creature.body;
		
		//check disabled
		
		for (p in body.parts)
		{
			for (m in p.muscles)
			{
				var muscle:Muscle = cast m;
				
				if (muscle.disable > 0)
				{
					body.setDisabled(muscle);
				}
			}
		}
		
		//check head
		
		if (body.parts.exists("Head") == true)
		{
			var headpart:BodyPart = cast body.parts.get("Head");
			if (headpart.muscles.exists("Brain"))
			{
				var brain:Muscle = cast headpart.muscles.get("Brain");
				if (brain.health <= 0)
				{
					creature.addStatusEffects(["DEATH"]);
					creature.removeStatusEffects(["UNCONSCIOUS"]);
				} else if (brain.disable > 0)
				{
					creature.addStatusEffects(["UNCONSCIOUS"]);
				} else
				{
					creature.removeStatusEffects(["UNCONSCIOUS", "DEATH"]);
				}
			}
		}
		
		if (body.parts.exists("Upper Torso") == true)
		{
			if (body.parts.get("Upper Torso").muscles.get("Heart").health <= 0)
			{
				creature.addStatusEffects(["DEATH"]);
			}
		}
		
		//check vision
		
		if (body.parts.exists("Head") == true)
		{
			var headpart:BodyPart = cast body.parts.get("Head");
			
			var righteyedead:Bool = false;
			
			if (headpart.muscles.exists("Right Eye"))
			{
				var righteye:Muscle = headpart.muscles.get("Right Eye");
				if (righteye.health <= 0 || righteye.disable > 0)
				{
					righteyedead = true;
				}
			}
			
			
			var lefteyedead:Bool = false;
			
			if (headpart.muscles.exists("Left Eye"))
			{
				var lefteye:Muscle = headpart.muscles.get("Left Eye");
				if (lefteye.health <= 0 || lefteye.disable > 0)
				{
					lefteyedead = true;
				}
			}
			
			if (righteyedead == true)
			{
				if (lefteyedead == true)
				{
					creature.addStatusEffects(["BLIND"]);
					creature.removeStatusEffects(["HALFBLINDRIGHT", "HALFBLINDLEFT"]);
				} else
				{
					creature.addStatusEffects(["HALFBLINDRIGHT"]);
					creature.removeStatusEffects(["BLIND", "HALFBLINDLEFT"]);
				}
			} else if (lefteyedead == true)
			{
					creature.addStatusEffects(["HALFBLINDLEFT"]);
					creature.removeStatusEffects(["BLIND", "HALFBLINDRIGHT"]);
			} else
			{
				creature.removeStatusEffects(["BLIND", "HALFBLINDRIGHT", "HALFBLINDLEFT"]);
			}
		}
		
		//check smell
		
		if (body.parts.exists("Head") == true)
		{
			var headpart:BodyPart = cast body.parts.get("Head");
			
			if (headpart.muscles.exists("Nose"))
			{
				var nose:Muscle = headpart.muscles.get("Nose");
				
				if (nose.health <= 0 || nose.disable > 0)
				{
					creature.addStatusEffects(["NOSMELL"]);
				}
			}
		}
		
		//check hearing
		
		if (body.parts.exists("Head") == true)
		{
			var headpart:BodyPart = cast body.parts.get("Head");
			
			var righteardead:Bool = false;
			
			if (headpart.muscles.exists("Right Ear"))
			{
				var rightear:Muscle = headpart.muscles.get("Right Ear");
				if (rightear.health <= 0 || rightear.disable > 0)
				{
					righteardead = true;
				}
			}
			
			
			var lefteardead:Bool = false;
			
			if (headpart.muscles.exists("Left Ear"))
			{
				var leftear:Muscle = headpart.muscles.get("Left Ear");
				if (leftear.health <= 0 || leftear.disable > 0)
				{
					lefteardead = true;
				}
			}
			
			if (righteardead == true)
			{
				if (lefteardead == true)
				{
					creature.addStatusEffects(["DEAF"]);
					creature.removeStatusEffects(["HALFDEAFRIGHT", "HALFDEAFLEFT"]);
				} else
				{
					creature.addStatusEffects(["HALFDEAFRIGHT"]);
					creature.removeStatusEffects(["DEAF", "HALFDEAFLEFT"]);
				}
			} else if (lefteardead == true)
			{
					creature.addStatusEffects(["HALFDEAFLEFT"]);
					creature.removeStatusEffects(["DEAF", "HALFDEAFRIGHT"]);
			} else
			{
				creature.removeStatusEffects(["DEAF", "HALFDEAFRIGHT", "HALFDEAFLEFT"]);
			}
		}
		
		//check bleeding
		
		var bleedcount:Float = 0;
		var bleeding:Bool = false;
		for (b in body.parts)
		{
			if (b.cuts > 0)
			{
				bleedcount += b.cuts * .1;
				
				bleeding = true;
			}
			
			for (m in b.muscles)
			{
				if (m.gash > 0 || m.cleave > 0 || m.stab > 0)
				{
					bleedcount += m.gash * 2;
					bleedcount += m.stab;
					bleedcount += m.cleave * 5;
					
					bleeding = true;
				}
			}
		}
		
		if (bleeding == true)
		{
			creature.bleedMultiplier = bleedcount / 5;
			creature.addStatusEffects(["BLEEDING"]);
		}
		
		//check breathing
		
		if (body.parts.exists("Upper Torso") == true)
		{
			var uppertorso:BodyPart = body.parts.get("Upper Torso");
			
			var rightlungdead:Bool = false;
			if (uppertorso.muscles.get("Right Lung").health <= 0)
			{
				rightlungdead = true;
			}
			
			var leftlungdead = false;
			if (uppertorso.muscles.get("Left Lung").health <= 0)
			{
				leftlungdead = true;
			}
			
			if (rightlungdead == true)
			{
				if (leftlungdead == true)
				{
					creature.addStatusEffects(["NOBREATH"]);
					creature.removeStatusEffects(["HALFBREATHRIGHT", "HALFBREATHLEFT"]);
				} else
				{
					creature.addStatusEffects(["HALFBREATHRIGHT"]);
					creature.removeStatusEffects(["NOBREATH", "HALFBREATHLEFT"]);
				}
			} else if (leftlungdead == true)
			{
					creature.addStatusEffects(["HALFBREATHLEFT"]);
					creature.removeStatusEffects(["NOBREATH", "HALFBREATHRIGHT"]);
			} else
			{
				creature.removeStatusEffects(["NOBREATH", "HALFBREATHRIGHT", "HALFBREATHLEFT"]);
			}
			
			//check consumption
			
			var stomachdead:Bool = false;
		
			if (uppertorso.muscles.exists("Stomach"))
			{
				var stomach:Muscle = uppertorso.muscles.get("Stomach");
				
				if (stomach.health <= 0)
				{
					creature.addStatusEffects(["NOEAT"]);
				} else
				{
					creature.removeStatusEffects(["NOEAT"]);
				}
			}
		}
		//check speaking
		
		if (body.parts.exists("Head") == true)
		{
			var headpart:BodyPart = cast body.parts.get("Head");
			
			if (headpart.muscles.exists("Jaw"))
			{
				var jaw:Muscle = headpart.muscles.get("Jaw");
				
				if (jaw.health <= 0 || jaw.disable > 0)
				{
					creature.addStatusEffects(["NOSPEAK"]);
				} else
				{
					creature.removeStatusEffects(["NOSPEAK"]);
				}
			}
		}		
		
		//check right arm
		
		var upperrightarmdead:Bool = false;
		
		if (body.parts.exists("Right Upper Arm") == true)
		{
			var upperrightarm:BodyPart = cast body.parts.get("Right Upper Arm");
			
			for ( m in upperrightarm.muscles)
			{
				var muscle:Muscle = cast m;
				
				if (muscle.health <= 0 || muscle.disable > 0)
				{
					upperrightarmdead = true;
					break;
				}
			}
		}
		
		var lowerrightarmdead:Bool = false;
		
		if (body.parts.exists("Right Lower Arm") == true)
		{
			var lowerrightarm:BodyPart = cast body.parts.get("Right Lower Arm");
			
			if (lowerrightarm.muscles.exists("Forearm"))
			{
				var forearm:Muscle = lowerrightarm.muscles.get("Forearm");
				
				if (forearm.health <= 0 || forearm.disable > 0)
				{
					lowerrightarmdead = true;
				}
			}
		}
		
		if (upperrightarmdead || lowerrightarmdead)
		{
			creature.addStatusEffects(["NOARMRIGHT"]);
		} else
		{
			creature.removeStatusEffects(["NOARMRIGHT"]);
		}
		
		// check left arm
		
		var upperleftarmdead:Bool = false;
		
		if (body.parts.exists("Left Upper Arm") == true)
		{
			var upperleftarm:BodyPart = cast body.parts.get("Left Upper Arm");
			
			for ( m in upperleftarm.muscles)
			{
				var muscle:Muscle = cast m;
				
				if (muscle.health <= 0 || muscle.disable > 0)
				{
					upperleftarmdead = true;
					break;
				}
			}
		}
		
		var lowerleftarmdead:Bool = false;
		
		if (body.parts.exists("Left Lower Arm") == true)
		{
			var lowerleftarm:BodyPart = cast body.parts.get("Left Lower Arm");
			
			if (lowerleftarm.muscles.exists("Forearm"))
			{
				var forearm:Muscle = lowerleftarm.muscles.get("Forearm");
				
				if (forearm.health <= 0 || forearm.disable > 0)
				{
					lowerleftarmdead = true;
				}
			}
		}
		
		if (upperleftarmdead || lowerleftarmdead)
		{
			creature.addStatusEffects(["NOARMLEFT"]);
		} else
		{
			creature.removeStatusEffects(["NOARMLEFT"]);
		}
		
		//check right hand
		
		var righthanddead:Bool = false;
		
		if (body.parts.exists("Right Lower Arm") == true)
		{
			var lowerrightarm:BodyPart = cast body.parts.get("Right Lower Arm");
			
			var fingercount:Int = 0;
			
			for (m in lowerrightarm.muscles)
			{
				var muscle:Muscle = cast m;
				
				if (muscle.name.indexOf("Finger") == -1 && muscle.name.indexOf("Forearm") == -1)
				{
					if (muscle.health <= 0 || muscle.disable > 0)
					{
						righthanddead = true;
						break;
					}
				} else
				{
					if (muscle.health <= 0 || muscle.disable > 0)
					{
						fingercount ++;
					}
				}
			}
			
			if (fingercount >= 2)
			{
				righthanddead = true;
			}
		}
		
		if (righthanddead)
		{
			creature.addStatusEffects(["NOHOLDRIGHT"]);
		} else
		{
			creature.removeStatusEffects(["NOHOLDRIGHT"]);
		}
		
		//check left hand
		
		var lefthanddead:Bool = false;
		
		if (body.parts.exists("Left Lower Arm") == true)
		{
			var lowerleftarm:BodyPart = cast body.parts.get("Left Lower Arm");
			
			var fingercount:Int = 0;
			
			for (m in lowerleftarm.muscles)
			{
				var muscle:Muscle = cast m;
				
				if (muscle.name.indexOf("Finger") == -1 && muscle.name.indexOf("Forearm") == -1)
				{
					if (muscle.health <= 0 || muscle.disable > 0)
					{
						lefthanddead = true;
						break;
					}
				} else
				{
					if (muscle.health <= 0 || muscle.disable > 0)
					{
						fingercount ++;
					}
				}
			}
			
			if (fingercount >= 2)
			{
				lefthanddead = true;
			}
		}
		
		if (lefthanddead)
		{
			creature.addStatusEffects(["NOHOLDLEFT"]);
		} else
		{
			creature.removeStatusEffects(["NOHOLDLEFT"]);
		}		
		
		
		//check legs
		//check crotch
	}
	
	public static function ApplyEffectsAfterHeal(creature:Creature):Void
	{
		var body:Body = creature.body;
		
		//check vision
		
	}
	
}