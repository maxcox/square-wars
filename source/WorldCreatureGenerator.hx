package;

import flixel.FlxG;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxMath;
import flixel.util.FlxRandom;
import haxe.ds.StringMap;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class WorldCreatureGenerator 
{
	
	public var genProgress:Int;
	
	// these variables are part of asyncRandomCreatures
	private var nextLeader:Int;
	private var currentLeader:WorldLeader;
	private var currentLeaderCount:Int;
	
	public function new() 
	{
		// initialize genProgress for asyncGenerateRegion
		genProgress = 0;
		
		nextLeader = 0;
		currentLeader = null;
		currentLeaderCount = 0;
	}
	
	public function asyncRandomCreatures(minimumPixelsX:Int, minimumPixelsY:Int, total:Int, container:FlxTypedGroup<WorldCreature> = null):FlxTypedGroup<WorldCreature>
	{
		var creatures:FlxTypedGroup<WorldCreature>;
		
		if (container != null)
		{
			creatures = container;
		} else
		{
			creatures = new FlxTypedGroup<WorldCreature>();
		}
		
		//trace(minimumPixelsX, minimumPixelsY);
		
		if (genProgress == 0)
		{
			nextLeader = FlxRandom.intRanged(0, Std.int(total / 25));
			
			currentLeader = null;
			currentLeaderCount = 0;
		}
		
		var genChunk:Int = Std.int(total / 50);
		
		for (g in 0...genChunk)
		{
			var i:Int = g + genProgress;
			
			var creature:Dynamic;
			
			if (i == nextLeader)
			{
				creature = new WorldLeader(FlxRandom.intRanged(minimumPixelsX, minimumPixelsX + WorldManager.regionSizePixels), FlxRandom.intRanged(minimumPixelsY,  minimumPixelsY + WorldManager.regionSizePixels));
				nextLeader = Std.int(i + creature.leadership * 2 + FlxRandom.intRanged(0, Std.int(total / 25)));
				
				if (Std.int(i + creature.leadership * 2) > total)
				{
					creature.leadership = Std.int((total - i) / 2);
				}
				
				//trace("LEADER");
				
				currentLeader = cast creature;
			} else
			{
				if (currentLeader != null)
				{	
					creature = new WorldCreature(currentLeader.x, currentLeader.y);
					creature.leader = currentLeader;
					
					if (currentLeaderCount < Std.int(currentLeader.leadership*2))
					{
						currentLeaderCount ++;
					} else
					{
						currentLeader = null;
						currentLeaderCount = 0;
					}
				} else
				{
					creature = new WorldCreature(FlxRandom.intRanged(minimumPixelsX, minimumPixelsX + WorldManager.regionSizePixels), FlxRandom.intRanged(minimumPixelsY,  minimumPixelsY + WorldManager.regionSizePixels));
				}
			}
			
			creatures.add(creature);
		}
		
		genProgress += genChunk;
		
		return creatures;
	}
	
	public function randomCreatures(minimumPixelsX:Int, minimumPixelsY:Int, total:Int, container:FlxTypedGroup<WorldCreature>=null):FlxTypedGroup<WorldCreature>
	{
		var creatures:FlxTypedGroup<WorldCreature>;
		
		if (container != null)
		{
			creatures = container;
		} else
		{
			creatures = new FlxTypedGroup<WorldCreature>();
		}
		
		trace(minimumPixelsX, minimumPixelsY);
		
		var nextLeader:Int = FlxRandom.intRanged(0, Std.int(total / 25));
		
		var currentLeader:WorldLeader = null;
		var currentLeaderCount:Int = 0;
		
		for (i in 0...total)
		{
			var creature:Dynamic;
			
			if (i == nextLeader)
			{
				creature = new WorldLeader(FlxRandom.intRanged(minimumPixelsX, minimumPixelsX + WorldManager.regionSizePixels), FlxRandom.intRanged(minimumPixelsY,  minimumPixelsY + WorldManager.regionSizePixels));
				nextLeader = Std.int(i + creature.leadership * 2 + FlxRandom.intRanged(0, Std.int(total / 25)));
				
				if (Std.int(i + creature.leadership * 2) > total)
				{
					creature.leadership = Std.int((total - i) / 2);
				}
				
				trace("LEADER");
				
				currentLeader = cast creature;
			} else
			{
				if (currentLeader != null)
				{	
					creature = new WorldCreature(currentLeader.x, currentLeader.y);
					creature.leader = currentLeader;
					
					if (currentLeaderCount < Std.int(currentLeader.leadership*2))
					{
						currentLeaderCount ++;
					} else
					{
						currentLeader = null;
						currentLeaderCount = 0;
					}
				} else
				{
					creature = new WorldCreature(FlxRandom.intRanged(minimumPixelsX, minimumPixelsX + WorldManager.regionSizePixels), FlxRandom.intRanged(minimumPixelsY,  minimumPixelsY + WorldManager.regionSizePixels));
				}
			}
			
			creatures.add(creature);
		}
		
		
		return creatures;
	}
	
}