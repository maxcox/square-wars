package ;
import flixel.FlxSprite;

/**
 * ...
 * @author Joseph Cox
 */
class Filter extends FlxSprite
{
	
	public function new(X:Float, Y:Float, ?SimpleGraphic:Dynamic):Void
	{
		super(X, Y, SimpleGraphic);
		scrollFactor.set(0, 0);
	}
	
}