package;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxPoint;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.Assets;

/**
 * ...
 * @author ...Joseph Cox
 */
class Effect extends FlxSprite
{

	public var firsttime:Bool;
	
	public var angleoff:Int;
	
	public var follow:FlxSprite;
	
	public var parent:FlxGroup;

	public function new(X:Int, Y:Int, W:Float, H:Float, Visual:String, follow:FlxSprite=null, angleoff:Int=0):Void 
	{
		super(X, Y);
		
		firsttime = false;
		
		var data:BitmapData = Assets.getBitmapData(Visual);
		var Length:Int = Std.int(data.width / 64);
		
		loadGraphic(Visual, true, 64, 64);
		
		var animationArray:Array<Int> = new Array<Int>();
		
		for (i in 0...Length)
		{
			animationArray.push(i);
		}
		
		animation.add("only", animationArray, 30, false);
		animation.play("only");
		
		this.angleoff = angleoff;
		
		if (follow != null)
		{
			this.follow = follow;
			
			if (angleoff != 0)
			{
				this.angleoff = angleoff;
			}
			
			updateFollow();
		}
		
		parent = Layers.EFFECTS;
		
		scale.x = W/48;
		scale.y = H/48;
	}	
	
	private function updateFollow():Void
	{
		x = follow.x - follow.width/2 - this.width/4;
		y = follow.y - follow.height / 2 - this.height / 4;
		
		angle = follow.angle;
		
		if (angleoff != 0)
		{
			x += Math.cos(Math.PI / 180 * (angle - 90)) * angleoff;
			y += Math.sin(Math.PI / 180 * (angle - 90)) * angleoff;
		}
	}
	
	override public function update():Void
	{
		if (follow != null)
		{
			updateFollow();
		}
		
		super.update();
		
		if (animation.finished == true)
		{
			parent.remove(this);
			this.destroy();
		}
	}
	
}