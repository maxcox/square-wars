package ;

/**
 * ...
 * @author ...
 */
class RegionInfo
{
	
	public var name:String;
	
	public var biome:String;

	public function new(name:String="Canadia", biome:String="grass"):Void
	{
		this.name = name;
		
		this.biome = biome;
	}
	
	public function randomize():RegionInfo
	{
		this.name = "USA";//Markov.getCountry();
		
		this.biome = "grass";
		
		return this;
	}
	
}