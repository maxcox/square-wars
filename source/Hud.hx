package  ;
import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.group.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.text.FlxTextField;
import flixel.util.FlxCollision;
import flixel.util.FlxColor;
import flixel.util.FlxPoint;
import flixel.util.FlxRect;
import openfl.display.Sprite;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class Hud extends FlxGroup
{

	private var player:Player;
	
	private var bloodbar:DisplayBar;
	private var oxygenbar:DisplayBar;
	private var adrenalinebar:DisplayBar;
	
	private var attackMenu:FlxGroup;
	
	private var tacticalTags:Array<FlxTextField>;
	
	public function new(player:Player):Void 
	{
		super();
		
		this.player = player;
		
		adrenalinebar = new DisplayBar(this, FlxG.width/2 - 35, 5, 30, 30, 0xFFFFFF00);
		oxygenbar = new DisplayBar(this, FlxG.width/2 - 70, 5, 30, 30, 0xFFACF8FF);
		bloodbar = new DisplayBar(this, FlxG.width/2 - 105, 5, 30, 30);
		
		attackMenu = null;
		
		tacticalTags = new Array<FlxTextField>();
	}
	
	/*
	public function openAttackMenu():Void
	{
		attackMenu = new FlxGroup();
		
		var mousepos:FlxPoint = FlxG.mouse.getWorldPosition();
		mousepos.x -= 33.5;
		mousepos.y -= 33.5;
		
		var head:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/head.png");
		var uppertorso:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/uppertorso.png");
		var lowertorso:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/lowertorso.png");
		var leftupperarm:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/leftupperarm.png");
		var rightupperarm:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/rightupperarm.png");
		var leftlowerarm:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/leftlowerarm.png");
		var rightlowerarm:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/rightlowerarm.png");
		var leftupperleg:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/leftupperleg.png");
		var rightupperleg:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/rightupperleg.png");
		var leftlowerleg:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/leftlowerleg.png");
		var rightlowerleg:FlxSprite = new FlxSprite(mousepos.x, mousepos.y, "assets/images/rightlowerleg.png");
		
		attackMenu.add(head);
		attackMenu.add(uppertorso);
		attackMenu.add(lowertorso);
		attackMenu.add(leftupperarm);
		attackMenu.add(rightupperarm);
		attackMenu.add(leftlowerarm);
		attackMenu.add(rightlowerarm);
		attackMenu.add(leftupperleg);
		attackMenu.add(rightupperleg);
		attackMenu.add(leftlowerleg);
		attackMenu.add(rightlowerleg);
		
		add(attackMenu);
	}
	
	public function closeAttackMenu():Void
	{
		remove(attackMenu);
		attackMenu = null;
	}
	
	public function attackUpdate():Void
	{
		if (attackMenu == null)
		{
			openAttackMenu();
		}
		
		if (FlxG.mouse.justReleased)
		{
			closeAttackMenu();
			player.attackpaused = false;
		}
	}
	*/
	
	public function tacticalUpdate():Void
	{
		super.update();
		
		function revealInfo(basic:FlxBasic):Void
		{
			if (Std.is(basic, Creature))
			{
				var creature:Creature = cast basic;
				
				var mousePoint:FlxPoint = FlxG.mouse.getWorldPosition();
				var mouseRect:FlxSprite = new FlxSprite(mousePoint.x - .5, mousePoint.y - .5);
				mouseRect.makeGraphic(1, 1, FlxColor.WHITE);
				
				if (FlxCollision.pixelPerfectCheck(mouseRect, creature.torso))
				{
					trace("name: " + creature.name);
					trace("blood: " + creature.blood / creature.MAXBLOOD);
					trace("weapon: " + cast(creature.equipment.get("weapon"), Weapon).checkStats());
					trace("status: " + creature.statusEffects);
					
					var blood:String = Std.string("blood: " + (Math.floor(100 * (creature.blood / creature.MAXBLOOD))) + "%");
					var oxygen:String = Std.string("oxygen: " + (Math.floor(100 * (creature.oxygen / creature.MAXOXYGEN))) + "%");
					var status:String = creature.statusEffects.toString();
					
					var tag:FlxTextField = new FlxTextField(creature.x - 5, creature.y - 5, 350, creature.name + "\n" + blood + "\n" + oxygen + "\n" + status);
					tag.setFormat(null, 8, 0x000000, "left", FlxText.BORDER_SHADOW);
					tacticalTags.push(tag);
					add(tag);
				}
			}
		}
		
		if (FlxG.mouse.justPressed)
		{
			CreatureManager.ActiveCreatures.forEachAlive(revealInfo);
		} else if (FlxG.mouse.justReleased)
		{
			function destroyTags(field:FlxTextField):Void
			{
				field.destroy();
			}
			
			for (i in tacticalTags)
			{
				destroyTags(i);
			}
			
			tacticalTags = new Array<FlxTextField>();
		}
	}
	
	override public function update():Void
	{
		super.update();
		
		bloodbar.update(player.blood, player.MAXBLOOD);
		oxygenbar.update(player.oxygen, player.MAXOXYGEN);
		adrenalinebar.update(player.adrenaline, player.MAXADRENALINE);
	}
	
	
}