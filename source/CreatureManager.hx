package  ;
import flixel.FlxBasic;
import flixel.FlxObject;
import flixel.group.FlxTypedGroup;

/**
 * ...
 * @author ...
 */
class CreatureManager
{
	
	public static var ActiveCreatures:FlxTypedGroup<Creature> = new FlxTypedGroup<Creature>();
	
	public function new() 
	{
		
	}
	
	public static function add(creature:Creature):Void
	{
		Layers.CREATURES.add(creature.bodyGroup);
		ActiveCreatures.add(creature);
	}
	
	public static function remove(creature:Creature):Void
	{
		Layers.CREATURES.remove(creature.bodyGroup);
		ActiveCreatures.remove(creature);
	}
	
}