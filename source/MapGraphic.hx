package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**
 * ...
 * @author Joseph Cox
 */
class MapGraphic extends FlxSprite
{

	public var parent:FlxGroup;
	
	public var graphic:String;

	public function new(X:Int, Y:Int, Visual:String, Parent:FlxGroup):Void 
	{
		super(X, Y, Visual);
		
		graphic = Visual;
		
		this.parent = Parent;
	}
}