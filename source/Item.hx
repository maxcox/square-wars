package ;

import flixel.FlxG;
import flash.geom.Point;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import flixel.util.FlxMath;
import flixel.FlxCamera;

/**
 * ...
 * @author Joseph Cox
 */
class Item extends FlxSprite
{

	public function new(X:Int, Y:Int, Visual:String) 
	{
		super(X, Y, "assets/images/" + Visual + "_world.png");
	}
}