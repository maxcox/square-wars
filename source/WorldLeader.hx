package ;

import flixel.FlxBasic;
import flixel.util.FlxColor;
import flixel.util.FlxRandom;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class WorldLeader extends WorldCreature
{
	
	// coordinates demonstrating the leader's current goal, the point that the leader and his/her followers will be travelling to, CURRENTLY UNUSED, MAY BE CHANGED
	public var goal:Point;
	
	// determines how many people will follow this leader
	public var leadership:Float;
		
	public function new(x:Int, y:Int, loading:Bool=false):Void
	{
		super(x, y, loading);
		
		// if this is a newly generated object, initialize everything as if new, otherwise if it's loaded do nothing because our load function will take care of the rest
		if (loading == false)
		{
			// generate a random leadership value based on our static min's and max's, this will determine how many followers this leader can have
			leadership = FlxRandom.floatRanged(WorldManager.leadermin, WorldManager.leadermax);
			
			// set goal as null for now, goals will be given out randomly in the process of updating
			goal = null;
		}
	}
	
	// loads a previously saved WorldLeader, assigning all loaded arguments to a new object's local variables
	public function loadLeader(x:Int, y:Int, homex:Int, homey:Int, range:Float, lastseen:Float=0, leadership:Int, goal:Point=null):WorldLeader
	{
		var loaded:WorldLeader = cast(load(x, y, homex, homey, range, lastseen), WorldLeader);
		
		loaded.leadership = leadership;
		
		if (goal != null)
		{
			loaded.goal = goal;
		} else
		{
			loaded.goal = null;
		}
		
		return loaded;
	}
	
}