package ;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxColor;
import flixel.util.FlxPoint;

/**
 * ...
 * @author ...
 */
class DisplayBar
{
	private var bar:FlxSprite;
	private var max:FlxSprite;
	
	private var parent:FlxGroup;
	
	public function new(parent:FlxGroup, x:Float=0, y:Float=0, width:Int=20, height:Int=20, color:Int=0xFFFF0000):Void 
	{
		
		max = new FlxSprite(x, y);
		max.makeGraphic(width+2, height+2, 0x99000000);
		max.scrollFactor.set(0, 0);
		parent.add(max);
		
		bar = new FlxSprite(x + 1, y + 1);
		bar.makeGraphic(width, height, color);
		bar.scrollFactor.set(0, 0);
		bar.origin.set(bar.width / 2, bar.height + 1);
		parent.add(bar);
		
		this.parent = parent;
	}
	
	public function remove():Void
	{
		parent.remove(max);
		parent.remove(bar);
	}
	
	public function update(value:Float, maxValue:Float):Void
	{
		bar.scale.y = Math.round((value / maxValue) * 10) / 10;
	}
	
}