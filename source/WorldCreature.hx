package ;

import flixel.FlxBasic;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxRandom;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class WorldCreature extends FlxBasic
{

	// the physical coordinates of the creature's home, or the point that they never wander "range" distance from
	public var homex:Int;
	public var homey:Int;
	
	// the creature's current coordinates in pixels
	public var x:Int;
	public var y:Int;
	
	// the maximum distance the creature will wander away from home
	public var range:Float;
	
	// the last time this creature was visually displayed
	public var lastseen:Float;
	
	// the creature that this creature follows, acts as a home position for roaming groups
	public var leader:WorldCreature;
	
	public var sprite:FlxSprite;
	
	public var home:FlxSprite;
	
	public var rendered:Bool;
		
	public function new(x:Int, y:Int, loading:Bool=false):Void
	{
		super();
		
		// set the creature's coordinates
		this.x = x;
		this.y = y;
		
		if (loading == false)
		{
			// set the homepoint as the creature's first coordinates
			homex = x;
			homey = y;
			
			// set range randomly based on the world manager's allowed range of ranges haha
			range = FlxRandom.floatRanged(WorldManager.rangemin, WorldManager.rangemax);
			
			// set lastseen's initial state
			lastseen = 0;
			
			// leader should be null until it is set from outside
			leader = null;
		}
		
		home = new FlxSprite(homex, homey);
		home.makeGraphic(64, 64, FlxColor.BROWN);
		
		rendered = false;
	}
	
	public function copy(x:Int, y:Int, homex:Int, homey:Int, range:Float, lastseen:Float = 0, leader:WorldCreature = null):Void
	{
		this.x = x;
		this.y = y;
		this.homex = homex;
		this.homey = homey;
		this.range = range;
		this.lastseen = lastseen;
		this.leader = leader;
	}
	
	// loads a previously saved WorldCreature, assigning all loaded arguments to a new object
	public function load(x:Int, y:Int, homex:Int, homey:Int, range:Float, lastseen:Float=0, leader:WorldCreature=null):WorldCreature
	{
		var loaded:WorldCreature = new WorldCreature(x, y, true);
		
		loaded.homex = homex;
		loaded.homey = homey;
		
		loaded.range = range;
		loaded.lastseen = lastseen;
		
		loaded.leader = leader;
		
		return loaded;
	}
	
}