package ;
import flixel.util.FlxMath;
import flixel.util.FlxRandom;

/**
 * ...
 * @author ...
 */
class NPC extends Creature
{

	public var aggressiveTowards:Array<Creature>;
	
	public var following:Creature;
	
	public var followrange:Int;
	
	public function new(X:Int, Y:Int, Visual:String):Void
	{
		super(X, Y, Visual);
		
		aggressiveTowards = new Array<Creature>();
		
		following = null;
		
		followrange = FlxRandom.intRanged(10, 80);
	}
	
	public function setFollowing(creature:Creature):Void
	{
		following = creature;
	}
	
	override public function update():Void
	{		
		super.update();
		
		if (following != null && CanISee() != 2)
		{
			var differencex:Float = following.x - x;
			var differencey:Float = following.y - y;

			if (differencex != 0)
			{
				direction.x = differencex;
			} else
			{
				direction.x = 0;
			}

			if (differencey != 0)
			{
				direction.y = differencey;
			} else
			{
				direction.y = 0;
			}
			
			if (Math.abs(differencex) > 100 + following.diagonal)
			{
				movement.x = FlxMath.signOf(differencex);
			} else if (Math.abs(differencex) < following.diagonal + followrange)
			{
				movement.x = -FlxMath.signOf(differencex);
			} else
			{
				movement.x = 0;
			}
			
			if (Math.abs(differencey) > 100 + following.diagonal)
			{
				movement.y = FlxMath.signOf(differencey);
			} else if (Math.abs(differencey) < following.diagonal + followrange)
			{
				movement.y = -FlxMath.signOf(differencey);
			} else
			{
				movement.y = 0;
			}
			
			HitManager.rangescale;
			
			if (Math.abs(differencex) < following.diagonal + 10 && Math.abs(differencey) < following.diagonal + 10)
			{
				if(FlxRandom.chanceRoll(1))
				{
					attack(FlxRandom.getObject(["high", "low"]));
				}
			}
		}
	}
	
}