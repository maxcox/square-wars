package ;
import flash.display.BitmapData;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxRandom;

/**
 * ...
 * @author Joseph Cox
 */
class Decals
{

	public function new() 
	{
		
	}
	
	public static function paint(x:Int, y:Int, decal:String, angle:Int=0, scale:Float=1):Void
	{
		if (scale > .3)
		{
			var extra:String = "assets/images/decal/";
			
			decal = extra + decal;
			
			var sprite:FlxSprite = new FlxSprite(x, y, decal);
			sprite.angle = angle;
			sprite.scale.x = scale;
			sprite.scale.y = scale;
			
			
			Layers.DECALS.add(sprite);
		}
	}
	
}