package ;

import body.Bodies;
import body.Body;
import body.BodyPart;
import body.Muscle;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.input.FlxAccelerometer;
import flixel.system.layer.frames.FlxFrame;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxVector;
import flixel.FlxBasic;
import flash.geom.Point;
import haxe.ds.StringMap;

/**
 * ...
 * @author Joseph Cox
 */
class Creature extends FlxSprite
{

	public var turnAngle:Float;
	
	public var type:String;
	public var name:String;
	
	public var conscious:Bool;

	public var defaultColor:Int;
	
	public var STRENGTH:Int;
	public var SPEED:Float;
	public var STEALTH:Int;
	
	public var currentSpeed:Float;
	public var speedMultiplier:Float;
	
	public var direction:FlxPoint;
	public var movement:FlxPoint;
	
	public var body:Body;
	
	public var bleedMultiplier:Float;
	public var blood:Float;
	public var MAXBLOOD:Float;
	
	public var oxygen:Float;
	public var MAXOXYGEN:Float;
	
	public var adrenaline:Float;
	public var MAXADRENALINE:Float;
	
	public var proficiencies:StringMap<Float>;
	
	public var healthManager:HealthManager;
	
	public var statusEffects:Array<String>;
	
	public var events:Array<String>;

	public var fallen:Bool;
	public var fallenMax:Int;
	public var fallenCount:Int;
	
	public var oldbalance:Point;
	public var balance:Point;
	public var balanceEffect:Point;
	public var moveBalanceEffect:Point;
	
	public var balanceAttackCap:Float;
	public var balanceMoveCap:Float;

	private var oldAnimation:String;
	public var currentAnimation:String;

	public var head:FlxSprite;
	public var torso:FlxSprite;
	public var legs:FlxSprite;

	public var attackSpeedCount:Float;
	public var attackSpeedMax:Float;
	
	public var defaultWeapon:Weapon;
	public var baseRange:Float;
	public var equipment:StringMap<Equipment>;

	public var bodyGroup:FlxGroup;

	public var realVelocity:FlxPoint;
	public var oldPosition:FlxPoint;
	
	public static var graphicSize:Int = 64;
	public var diagonal:Float; //FOR HIT TESTING
	
	public var running:Bool;
	
	public function new(X:Int, Y:Int, Visual:String):Void
	{
		super(X, Y, "assets/images/" + Visual + "_mid.png");

		defaultColor = 0xf4d18f;

		equipment = new StringMap<Equipment>();

		oldbalance = new Point(0, 0);
		balance = new Point(0, 0);
		balanceEffect = new Point(0, 0);
		moveBalanceEffect = new Point(0, 0);
		
		balanceAttackCap = 7;
		balanceMoveCap = 3;

		currentAnimation = "idle";

		head = new FlxSprite( X, Y);
		head.loadGraphic("assets/images/" + Visual + "_head.png", false, graphicSize, graphicSize);
		head.animation.add("idle", [0], 8, false);
		head.animation.add("walk", [1,0,2,0], 8, true);
		head.animation.add("sidewalk", [3,0,4,0], 8, true);

		torso = new FlxSprite(X, Y);
		torso.loadGraphic("assets/images/" + Visual + "_torso.png", true, graphicSize, graphicSize);
		torso.animation.add("idle", [0], 8, false);
		torso.animation.add("walk", [1,0,2,0], 8, true);
		torso.animation.add("sidewalk", [3,0,4,0], 8, true);

		legs = new FlxSprite(X, Y);
		legs.loadGraphic("assets/images/" + Visual + "_legs.png", true, graphicSize, graphicSize);
		legs.animation.add("idle",[0], 8, false);
		legs.animation.add("walk", [1,0,2,0], 8, true);
		legs.animation.add("sidewalk", [3,2,4,2], 8, true);

		bodyGroup = new FlxGroup();

		bodyGroup.add(legs);
		bodyGroup.add(this);
		bodyGroup.add(torso);
		bodyGroup.add(head);

		legs.allowCollisions = 0;
		torso.allowCollisions = 0;
		head.allowCollisions = 0;

		setDefaultStats();
		
		bleedMultiplier = 1;
		
		proficiencies = new StringMap<Float>();
		
		direction = new FlxPoint(0, 0);
		movement = new FlxPoint(0,0);
		
		drag.set(5000, 5000);
		
		events = new Array<String>();
		statusEffects = new Array<String>();
		
		body = Bodies.ReturnHumanBody();
		
		width = width / 4;
		height = height / 4;
		centerOffsets();

		head.width = torso.width = legs.width = width;
		head.height = torso.height = legs.height = height;
		head.offset.x = torso.offset.x = legs.offset.x = offset.x;
		head.offset.y = torso.offset.y = legs.offset.y = offset.y;

		realVelocity = new FlxPoint(0,0);
		oldPosition = new FlxPoint(x,y);

		conscious = true;
		
		fallen = false;
		fallenMax = 100;
		fallenCount = 0;

		set_color(defaultColor);
		
		attackSpeedCount = 10.0;
		attackSpeedMax = 10.0;
		
		baseRange = 1;
		defaultWeapon = new Weapon(0, 0, "testsword", [.25, .25, .25], 1, .5, Materials.skin, Weapon.FIST);
		
		diagonal = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
		
		turnAngle = angle;
		
		currentSpeed = SPEED;
		speedMultiplier = 1;
		running = false;
	}
	
	public function setDefaultStats():Void
	{
		STRENGTH = 4;
		SPEED = 4;
		STEALTH = 4;
		
		MAXBLOOD = 100;
		MAXOXYGEN = 100;
		MAXADRENALINE = 100;
		
		blood = MAXBLOOD;
		oxygen = MAXOXYGEN;
		adrenaline = MAXADRENALINE;
	}
	
	public function setRandomStats():Void
	{
		STRENGTH = FlxRandom.intRanged(1,10);
		SPEED = FlxRandom.floatRanged(3,5);
		STEALTH = FlxRandom.intRanged(1, 10);
		
		MAXBLOOD = FlxRandom.intRanged(50, 120);
		MAXOXYGEN = FlxRandom.intRanged(50, 120);
		MAXADRENALINE = FlxRandom.intRanged(20, 100);
		
		blood = MAXBLOOD;
		oxygen = MAXOXYGEN;
		adrenaline = MAXADRENALINE;
		
		trace("Speed is: " + SPEED);
	}
	
	public function equip(equip:Equipment):Void
	{
		var equipSpot:String = "";

		if (Std.is(equip, Weapon))
		{
			equipSpot = "weapon";
		} else if (Std.is(equip, Armor))
		{
			var armor:Armor = cast(equip);

			equipSpot = armor.type;
		}

		if (equipment.exists(equipSpot))
		{
			unequip(equipSpot);
		}

		if (equipSpot == "torso")
		{
			this.set_color(cast(equip, Armor).midColor);
		}

		equipment.set(equipSpot, equip);

		Layer();

		equip.sprite.width = width;
		equip.sprite.height = height;
		equip.sprite.offset = offset;
		
		RenderBalance();

		function restart(basic:FlxBasic):Void
		{
			var sprite:FlxSprite = cast(basic);

			if(sprite.animation.curAnim != null)
			{
				sprite.animation.curAnim.restart();
			}
		}

		bodyGroup.forEach(restart);

		equip.sprite.animation.play(currentAnimation, true);
	}

	public function unequip(spot:String):Void
	{
		if (equipment.exists(spot))
		{
			var equip:Equipment = equipment.get(spot);

			equipment.remove(spot);

			bodyGroup.remove(equip.sprite);

			if (spot == "torso")
			{
				set_color(defaultColor);
			}
		}
	}

	public function checkEvent(event:String):Bool
	{
		var hasEvent:Bool = false;
		
		if (Lambda.indexOf(events, event) == 1)
		{
			hasEvent = true;
		}
		
		return hasEvent;
	}
	
	public function checkStatus(status:String):Bool
	{
		var hasStatus:Bool = false;
		
		if (Lambda.indexOf(statusEffects, status) != -1)
		{
			hasStatus = true;
		}
		
		return hasStatus;
	}
	
	public function addStatusEffects(statuses:Array<String>):Void
	{
		for (i in statuses)
		{
			var status:String = i;
			
			if (checkStatus(status) == false)
			{
				statusEffects.push(status);
				
				trace(name + " has " + status + "!");
			}
		}
	}
	
	public function removeStatusEffects(statuses:Array<String>):Void
	{
		for (i in statuses)
		{
			var status:String = i;
			
			if (checkStatus(status) == true)
			{
				statusEffects.remove(status);
				
				trace(statusEffects);
			}
		}
	}
	
	public function Animation():Void
	{
		var dirAngle:Float = Math.atan2(direction.y, direction.x) * 180 / Math.PI;
		var moveAngle:Float = Math.atan2(realVelocity.y, realVelocity.x) * 180 / Math.PI;

		if (realVelocity.x != 0 || realVelocity.y != 0)
		{
			var angleDiff:Float = Math.abs(dirAngle - moveAngle);
			
			if (angleDiff <= 45 || (angleDiff >= 135 && angleDiff <= 215) || angleDiff >= 315)
			{
				currentAnimation = "walk";
				
				if (angleDiff >= 135 && angleDiff <= 215)
				{
					speedMultiplier = .5;
				} else
				{
					speedMultiplier = 1;
				}
			} else
			{
				currentAnimation = "sidewalk"; 
				
				speedMultiplier = .7;
			}
		} else
		{
			currentAnimation = "idle";
		}

		doAnimation();
	}

	private function doAnimation():Void
	{

		legs.animation.play(currentAnimation);
		torso.animation.play(currentAnimation);
		head.animation.play(currentAnimation);

		for (i in equipment)
		{
			i.sprite.animation.play(currentAnimation);
		}

		if (oldAnimation != currentAnimation && legs.animation.curAnim != null)
		{

			legs.animation.curAnim.restart();
			torso.animation.curAnim.restart();
			head.animation.curAnim.restart();

			for (i in equipment)
			{
				i.sprite.animation.curAnim.restart();
			}
		}

		oldAnimation = currentAnimation;
	}

	private function AnimBob():Void
	{
		if (torso.animation.frameIndex == 0)
		{
			function scaleUp(basic:FlxBasic):Void
			{
				var sprite:FlxSprite = cast(basic);

				sprite.scale.x = 1.1;
				sprite.scale.y = 1.1;
			}

			bodyGroup.forEach(scaleUp);
		} else
		{
			function scaleDown(basic:FlxBasic):Void
			{
				var sprite:FlxSprite = cast(basic);

				sprite.scale.x = 1;
				sprite.scale.y = 1;
			}

			bodyGroup.forEach(scaleDown);
		}
	}
	
	public function attack(attackheight:String="high"):Void
	{
		if (CanIAttack())
		{
			if (attackSpeedCount >= attackSpeedMax)
			{
				var oxygencost:Float = 5;
				
				if (oxygen >= oxygencost)
				{
					var attackchangex:Float = 4 * Math.cos(angle * Math.PI / 180 - 90);
					var attackchangey:Float = 4 * Math.sin(angle * Math.PI / 180 - 90);
					
					if (Math.sqrt(Math.pow(balanceEffect.x, 2) + Math.pow(balanceEffect.y, 2)) + Math.sqrt(Math.pow(attackchangex,2) + Math.pow(attackchangey,2)) < balanceAttackCap)
					{
						balanceEffect.x += attackchangex;
						balanceEffect.y += attackchangey;
					}
					
					var weapon:Weapon = defaultWeapon;
					
					if (equipment.exists("weapon") == true)
					{
						weapon = cast equipment.get("weapon");
					}
					
					var atktype:String = FlxRandom.getObject(weapon.options);
					
					HitManager.HitAnythingTest(this, weapon, atktype, attackheight);
					
					oxygen -= oxygencost;
				}
				
				attackSpeedCount = 0.0;
			}
		}
	}

	private function Move():Void
	{
		if ( movement.x != 0 || movement.y != 0)
		{
			var horizontalSpeed:Float = 0;
			var verticalSpeed:Float = 0;
			
			if (running == false)
			{
				speedMultiplier = 1;
			}
			
			if (movement.x == -1)
			{
				horizontalSpeed -= currentSpeed * speedMultiplier;
			} else if (movement.x == 1)
			{
				horizontalSpeed += currentSpeed * speedMultiplier; 	
			}
			
			if (movement.y == -1)
			{
				verticalSpeed -= currentSpeed * speedMultiplier;
			} else if (movement.y == 1)
			{
				verticalSpeed += currentSpeed * speedMultiplier;
			}
			
			if (horizontalSpeed != 0 && verticalSpeed != 0)
			{
				var diagonal:Float = MathBucket.RTPConst;
				
				horizontalSpeed *= diagonal;
				verticalSpeed *= diagonal;
			}
			
			var velocityVector:Float = FlxMath.vectorLength(velocity.x, velocity.y);
			
			acceleration.x = movement.x * (velocityVector + 800);
			acceleration.y = movement.y * (velocityVector + 800);
			
			//velocity.x += horizontalSpeed * 50;
			//velocity.y += verticalSpeed * 50;
			oxygen -= .20;
			
			
			maxVelocity.x = Math.abs(horizontalSpeed * 50);
			maxVelocity.y = Math.abs(verticalSpeed * 50);
		} else
		{
			acceleration.x = 0;
			acceleration.y = 0;
		}
	}
	
	public function BalanceReturn():Void
	{
		var balanceLength:Float = Math.sqrt(balanceEffect.x * balanceEffect.x + balanceEffect.y * balanceEffect.y);
		var balanceAngle:Float = Math.atan2(balanceEffect.y, balanceEffect.x);
		
		if (balanceLength > .2)
		{
			balanceEffect.x -= Math.cos(balanceAngle) * .2;
			balanceEffect.y -= Math.sin(balanceAngle) * .2;
		} else
		{
			balanceEffect.x = 0;
			balanceEffect.y = 0;
		}
	}
	
	private function Turn():Void
	{
		var testangle:Float = angle;
		
		if (testangle > 360)
		{
			testangle %= 360;
		} else if (testangle < 0)
		{
			testangle += 360;
		}
		
		if (turnAngle > 360)
		{
			turnAngle = turnAngle % 360;
		} else if (turnAngle < 0)
		{
			turnAngle += 360;
		}
		
		var diff1:Float = ((turnAngle - testangle + 180) % 360) - 180;
		var absdiff:Float = Math.abs(diff1);
		
		if (absdiff < 180 && absdiff > 20)
		{
			angularVelocity = FlxMath.signOf(diff1) * (absdiff * 10 + 500);
		} else if (absdiff > 20 && absdiff < 340)
		{
			angularVelocity = -FlxMath.signOf(diff1) * (absdiff * 10 + 500);
		} else
		{
			angle = turnAngle;
			angularVelocity = 0;
		}
	}
	
	public function ComputeBalance():Void
	{
		var balanceAccel:Float = .2;
		
		var movementaddx:Float = movement.x * balanceAccel;
		var movementaddy:Float = movement.y * balanceAccel;
		
		var movementangle:Float = Math.atan2(movement.y, movement.x); 
		
		var limitx:Float = balanceMoveCap * Math.cos(movementangle);
		var limity:Float = balanceMoveCap * Math.sin(movementangle);
		
		var moveBalanceLength:Float = Math.sqrt(moveBalanceEffect.x * moveBalanceEffect.x + moveBalanceEffect.y * moveBalanceEffect.y);
		var moveBalanceAngle:Float = Math.atan2(moveBalanceEffect.y, moveBalanceEffect.x);
		
		if (movementaddx != 0)
		{
			if (Math.abs(moveBalanceEffect.x + movementaddx) < Math.abs(limitx))
			{
				moveBalanceEffect.x += movementaddx;
			}
		} else
		{
			if (moveBalanceLength > balanceAccel)
			{
				moveBalanceEffect.x -= Math.cos(moveBalanceAngle) * balanceAccel;
			} else
			{
				moveBalanceEffect.x = 0;
			}
		}
			
		if (movementaddy != 0)
		{
			if (Math.abs(moveBalanceEffect.y + movementaddy) < Math.abs(limity))
			{
				moveBalanceEffect.y += movementaddy;
			}
		} else
		{
			if (moveBalanceLength > balanceAccel)
			{
				moveBalanceEffect.y -= Math.sin(moveBalanceAngle) * balanceAccel;
			} else
			{
				moveBalanceEffect.y = 0;
			}
		}
		
		balance.x = balanceEffect.x + moveBalanceEffect.x;
		balance.y = balanceEffect.y + moveBalanceEffect.y;
		
		if (FlxMath.vectorLength(balance.x, balance.y) > balanceAttackCap + balanceMoveCap + 1)
		{
			fallen = true;
			trace(name + " HAS FALLEN");
			balance.x = 0;
			balance.y = 0;
			balanceEffect.x = 0;
			balanceEffect.y = 0;
		}
	}

	public function RenderBalance():Void
	{
		//var elapsedTime:Int = flash.Lib.getTimer();
		BalanceReturn();
		
		ComputeBalance();
		//trace(flash.Lib.getTimer() - elapsedTime);
		
		head.x = torso.x = legs.x = x;
		head.y = torso.y = legs.y = y;
		head.angle = torso.angle = legs.angle = angle;

		for (i in equipment)
		{
			i.sprite.x = x + balance.x * i.layeroff;
			i.sprite.y = y + balance.y * i.layeroff;
			i.sprite.angle = angle;
		}

		head.x += balance.x * 1;
		head.y += balance.y * 1;

		torso.x += balance.x * .5;
		torso.y += balance.y * .5;

		legs.x -= balance.x * .5;
		legs.y -= balance.y * .5;

		turnAngle = Math.atan2(direction.y, direction.x) * 180 / Math.PI + 90;
		
		oldbalance.x = balance.x;
		oldbalance.y = balance.y;
	}
	
	private function AmIAlive():Bool
	{
		var alive:Bool = true;
		
		if (checkStatus("DEATH") == true)
		{
			alive = false;
		}
			
		return alive;
	}
	
	private function AmIConscious():Bool
	{
		if (checkStatus("UNCONSCIOUS") == true)
		{
			return false;
		} else
		{
			return true;
		}
	}
	
	private function AmIBleeding():Bool 
	{
		if (checkStatus("BLEEDING") == true)
		{
			return true;
		}
		
		return false;
	}
	
	private function CanIAttack():Bool
	{
		if (checkStatus("NOARMLEFT") || checkStatus("NOHOLDLEFT"))
		{
			return false;
		} else
		{
			return true;
		}
	}
	
	
	
	private function CanIBreathe():Int
	{
		if (checkStatus("NOBREATH") == true)
		{
			return 2;
		} else if (checkStatus("HALFBREATHRIGHT") == true || checkStatus("HALFBREATHLEFT") == true)
		{
			return 1;
		} else
		{
			return 0;
		}
	}
	
	private function CanISee():Int
	{
		if (checkStatus("BLIND") == true)
		{
			return 2;
		} else if (checkStatus("HALFBLINDRIGHT") == true || checkStatus("HALFBLINDLEFT") == true)
		{
			return 1;
		} else
		{
			return 0;
		}
	}
	
	private function CanIMove():Bool
	{
		return true;
	}

	public function setVelocity():Void
	{
		realVelocity.x = x - oldPosition.x;
		realVelocity.y = y - oldPosition.y;

		oldPosition.x = x;
		oldPosition.y = y;
	}

	public function Layer():Void
	{

		function remove(basic:FlxBasic):Void
		{
			bodyGroup.remove(basic);
		}

		bodyGroup.forEach(remove);

		bodyGroup.add(legs);
		if (equipment.exists("legs"))
		{
			bodyGroup.add(equipment.get("legs").sprite);
		}
		bodyGroup.add(this);
		if (equipment.exists("weapon"))
		{
			bodyGroup.add(equipment.get("weapon").sprite);
		}
		bodyGroup.add(torso);
		if (equipment.exists("torso"))
		{
			bodyGroup.add(equipment.get("torso").sprite);
		}
		bodyGroup.add(head);

	}
	
	override public function update():Void
	{
		setVelocity();
		
		super.update();
		
		x = Math.round(x);
		y = Math.round(y);
		
		if (AmIAlive() == true)
		{
			if (AmIConscious() == true && fallen == false)
			{
				if (CanISee() == 2 && Std.is(this, Player))
				{
					var blindFilter:Filter = new Filter(FlxG.camera.x, FlxG.camera.y);
					blindFilter.makeGraphic(FlxG.camera.width, FlxG.camera.height, FlxColor.BLACK);
					
					Layers.FILTERS.add(blindFilter);
					
					trace("BLINDED");
				}
				
				if (CanIMove() == true && oxygen > 0)
				{
					RenderBalance();
					Animation();
					AnimBob();
					Move();
					Turn();
				}			
				
				if (AmIBleeding() == true)
				{
					blood -= .005 * bleedMultiplier;
					
					if (blood <= 0)
					{
						addStatusEffects(["DEATH"]);
					}
					
					if (FlxRandom.chanceRoll(bleedMultiplier) == true)
					{
						var sizemin:Float = .1;
						var sizemax:Float = 1.0;
						var size:Float = bleedMultiplier * .1;
						
						if (size > 1.0)
						{
							size = 1.0;
						} else if (size < .4)
						{
							size = .4;
						}
						
						Decals.paint(Std.int(x - width / 2 + FlxRandom.intRanged(Std.int(-width/2),Std.int(width/2))), Std.int(y - height / 2 + FlxRandom.intRanged(Std.int(-height/2),Std.int(height/2))), "bloodtile1.png", FlxRandom.intRanged(0, 360), FlxRandom.floatRanged(0.4, size));
					}
				}
				
				if (oxygen < MAXOXYGEN)
				{
					var rate:Float = .25;
					if (CanIBreathe() == 1)
					{
						rate = .125;
					} else if (CanIBreathe() == 2)
					{
						rate = 0;
					}
					
					oxygen += rate;
					if (oxygen > MAXOXYGEN)
					{
						oxygen = MAXOXYGEN;
					}
				}
				
				if (attackSpeedCount < attackSpeedMax)
				{
					if (equipment.exists("weapon"))
					{
						var weapon:Weapon = cast equipment.get("weapon");
						attackSpeedCount += weapon.speed;
					} else
					{
						attackSpeedCount += defaultWeapon.speed;
					}
				}
			} else if (fallen)
			{
				if (fallenCount < fallenMax)
				{
					fallenCount ++;
				} else
				{
					fallenCount = 0;
					fallen = false;
				}
			}
			
			body.update();
			if (body.checkflag)
			{
				Capabilities.ApplyEffectsAfterDamage(this);
				
				body.checkflag = false;
			}
		} else
		{
			kill();
			bodyGroup.kill();
		}
	}
}