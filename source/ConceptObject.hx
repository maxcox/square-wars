package ;

/**
 * ...
 * @author Joseph Cox
 */
class ConceptObject
{

	public var name:String;
	
	public var alive:Bool;
	
	public var gender:String;
	
	public var properties:Array<String>;
	
	public function new(name:String, alive:Bool, gender:String, properties:Array<String>) 
	{
		this.name = name;
		
		this.alive = alive;
		
		this.gender = gender;
		
		this.properties = properties;
	}
	
	public function setName(name:String):Void
	{
		this.name = name;
	}
	
	public function setProperties(props:Array<String>):Void
	{
		properties = props;
	}
	
	public function setGender(gender:String):Void
	{
		this.gender = gender;
	}
	
	public function setAlive(alive:Bool):Void
	{
		this.alive = alive;
	}
	
}